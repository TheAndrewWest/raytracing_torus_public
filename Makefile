CXX = g++
IDIR = ./include
CXXFLAGS = --std=c++11 -O3 -Wall -march=native -I $(IDIR)
LDFLAGS = -lm
SOURCES = aliev.cpp basis.cpp glampedakis.cpp kerr.cpp metric.cpp pani.cpp torus.cpp xTrackCode.cpp
OBJECTS = $(SOURCES:.cpp=.o)
EXECUTABLE = xTrackCode
DEPDIR := .d
DEPFLAGS = -MT $@ -MMD -MP -MF $(DEPDIR)/$*.Td
DEBUG := 0

DEBUGFLAGS = 
ifeq (${DEBUG},1)
DEBUGFLAGS = -g
endif

$(shell mkdir -p $(DEPDIR) >/dev/null)


COMPILE.cc = $(CXX) $(DEPFLAGS) $(CXXFLAGS) $(CPPFLAGS) $(DEBUGFLAGS) -c
POSTCOMPILE = @mv -f $(DEPDIR)/$*.Td $(DEPDIR)/$*.d && touch $@



all: $(SOURCES) $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS)
	$(CXX) $(LDFLAGS) $(OBJECTS) -o $@

%.o : %.cpp
%.o : %.cpp $(DEPDIR)/%.d
	$(COMPILE.cc) $(OUTPUT_OPTION) $<
	$(POSTCOMPILE)

$(DEPDIR)/%.d: ;
.PRECIOUS: $(DEPDIR)/%.d

include $(wildcard $(patsubst %,$(DEPDIR)/%.d,$(basename $(SOURCES))))

clean:
	rm *.o
	rm -rf .d
	rm $(EXECUTABLE)
