//Written by A.T. West

#include "basis.h"
#include <stdlib.h>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <cmath>
#include <math.h>


using namespace std;


//finds dot product of two 4-vectors with metric
//all these results were verified against Mathematica notebook
double Basis::dot4v(double gmunu[4][4], double g00, double g03, double g11, double g22, double g33, double vec1[4], double vec2[4])
{
    double sum4=0.0;
    double gv[4];
    
    //clear gmunu
    for (int i =0; i<4; i++){
        for (int j =0; j<4; j++){
            gmunu[i][j]=0.;
        }
        gv[i]=0.0;
    }
    gmunu[0][0]=g00;
    gmunu[0][3]=g03;
    gmunu[1][1]=g11;
    gmunu[2][2]=g22;
    gmunu[3][0]=g03;
    gmunu[3][3]=g33;
    
    for (int k =0; k<4; k++){
        for (int g=0; g<4; g++){
            gv[k] += gmunu[k][g]*vec1[g];
        }
    }
    for (int h=0; h<4; h++){
        sum4 += gv[h]*vec2[h];

    }
    //cout << "dot product = " << sum4 << endl;
    return sum4;
};


/*
The ordering of this process is defined for the torus process where the third tangent vector is unknown.
 Further refinement is needed for arbitrary application
 The function argumements could also probably be simplified for readability.
 */
//void Basis::gsOrthog(double gmunu[4][4], double g00, double g03, double g11, double g22, double g33, double t0[4], double t1[4], double t2[4], double t3[4], double emu[4][4])
void Basis::gsOrthog(double gmunu[4][4],double t0[4], double t1[4], double t2[4], double t3[4], double emu[4][4])
{
    double e1p[4];
    double e2p[4];
    double e3p[4];
    double emuT[4][4]; //vectors emu are column vectors - this matrix will be filled and transposed to give emu[4][4]
    
    //clear gmunu and e1p, e2p, e3p
    for (int i =0; i<4; i++){
        for (int j =0; j<4; j++){
//            gmunu[i][j]=0;
            emu[i][j]=0;
            emuT[i][j]=0;
        }
        e1p[i] = 0.0;
        e2p[i] = 0.0;
        e3p[i] = 0.0;
    }
    double g00=gmunu[0][0];
    double g03=gmunu[0][3];
    double g11=gmunu[1][1];
    double g22=gmunu[2][2];
//  double   g03=gmunu[3][0];
    double g33=gmunu[3][3];
    
    
    //find first normalized basis vector
    for (int k=0; k<4; k++)
    {
        emuT[0][k] = t0[k]/sqrt(abs(dot4v(gmunu,g00,g03,g11,g22,g33,t0,t0)));
    }
    //find second normalized basis vector orthogonal to e0
    for (int l=0;l<4;l++)
    {
        e1p[l] = t1[l]-((dot4v(gmunu,g00,g03,g11,g22,g33,t1,emuT[0]))/abs((dot4v(gmunu,g00,g03,g11,g22,g33,emuT[0],emuT[0]))))*emuT[0][l];
        cout <<"e1p = " << e1p[l] << endl;
    }
    for (int m=0;m<4;m++)
    {
        emuT[1][m] = e1p[m]/sqrt((dot4v(gmunu,g00,g03,g11,g22,g33,e1p,e1p)));
    }
    
    //find fourth normalized basis vector orthogonal to e0,e1
    for (int n=0;n<4;n++)
    {
        e3p[n] = t3[n] - (dot4v(gmunu,g00,g03,g11,g22,g33,t3,emuT[0])/abs((dot4v(gmunu,g00,g03,g11,g22,g33,emuT[0],emuT[0]))))*emuT[0][n] - (dot4v(gmunu,g00,g03,g11,g22,g33,t3,emuT[1])/(dot4v(gmunu,g00,g03,g11,g22,g33,emuT[1],emuT[1])))*emuT[1][n];
    }
    for (int p=0;p<4;p++)
    {
        emuT[3][p] = e3p[p]/sqrt(dot4v(gmunu,g00,g03,g11,g22,g33,e3p,e3p));
    }
    
    //find third normalized basis vector orthogonal to e0,e1,e3
    for (int q=0; q<4;q++)
    {
        
        e2p[q] = t2[q]-(dot4v(gmunu,g00,g03,g11,g22,g33,t2,emuT[0])/abs((dot4v(gmunu,g00,g03,g11,g22,g33,emuT[0],emuT[0]))))*emuT[0][q] - (dot4v(gmunu,g00,g03,g11,g22,g33,t2,emuT[1])/(dot4v(gmunu,g00,g03,g11,g22,g33,emuT[1],emuT[1])))*emuT[1][q] - (dot4v(gmunu,g00,g03,g11,g22,g33,t2,emuT[3])/(dot4v(gmunu,g00,g03,g11,g22,g33,emuT[3],emuT[3])))*emuT[3][q];
    }
    
    for (int r=0;r<4;r++)
    {
        emuT[2][r] = e2p[r]/sqrt(dot4v(gmunu,g00,g03,g11,g22,g33,e2p,e2p));
    }
    
    
    // Transpose emuT so that the vectors are column vectors
    for (int i =0; i<4; i++){
        for (int j =0; j<4; j++){
            emu[j][i] = emuT[i][j];
        }
    }
    
    //variables for debugging, leaving in for use in further function refinement.
    //will be removed before changes push to master
//    for (int s=0;s<4;s++)
//    {
//        cout << "t1["<<s<<"] = "<< t1[s] << endl;
//        cout << "e1p["<<s<<"] = "<< e1p[s] << endl;
//        for (int t=0;t<4;t++)
//        {
//            cout << "emuT["<<s<<"]["<<t<<"] = "<< emuT[s][t]<<endl;
//        }
//    }
//
//
//
//
    double e01 = 0.0;
    double e02 = 0.0;
    double e03 = 0.0;
    double e12 = 0.0;
    double e13 = 0.0;
    double e23 = 0.0;

    double e00 = 0.0;
    double e11 = 0.0;
    double e22 = 0.0;
    double e33 = 0.0;
//
//
//
        e01 = dot4v(gmunu,g00,g03,g11,g22,g33,emuT[0],emuT[1]);
        e02 = dot4v(gmunu,g00,g03,g11,g22,g33,emuT[0],emuT[2]);
        e03 = dot4v(gmunu,g00,g03,g11,g22,g33,emuT[0],emuT[3]);
        e12 = dot4v(gmunu,g00,g03,g11,g22,g33,emuT[1],emuT[2]);
        e13 = dot4v(gmunu,g00,g03,g11,g22,g33,emuT[1],emuT[3]);
        e23 = dot4v(gmunu,g00,g03,g11,g22,g33,emuT[2],emuT[3]);

        e00 = dot4v(gmunu,g00,g03,g11,g22,g33,emuT[0],emuT[0]);
        e11 = dot4v(gmunu,g00,g03,g11,g22,g33,emuT[1],emuT[1]);
        e22 = dot4v(gmunu,g00,g03,g11,g22,g33,emuT[2],emuT[2]);
        e33 = dot4v(gmunu,g00,g03,g11,g22,g33,emuT[3],emuT[3]);




        if (e00 != -1)
        {
            cout << "e0 not normalized " << e00 << endl;
        }

        if (e11 != 1)
        {
            cout << "e1 not normalized " << e11 << endl;
        }
        if (e22 != 1)
        {
            cout << "e2 not normalized " << e22 << endl;
        }
        if (e33 != 1)
        {
            cout << "e3 not normalized " << e33 << endl;
        }


        if (abs(e01) > 0.00000000000001)
        {
            cout << "e01 NOT ORTHOG" << e01 << endl;
        }

        if (abs(e02) > 0.00000000000001)
        {
            cout << "e02 NOT ORTHOG" << e02 << endl;
        }
        if (abs(e03) > 0.00000000000001)
        {
            cout << "e03 NOT ORTHOG" << e03 << endl;
        }
        if (abs(e12) > 0.00000000000001)
        {
            cout << "e12 NOT ORTHOG" << e12 << endl;
        }
        if (abs(e13) > 0.00000000000001)
        {
            cout << "e13 NOT ORTHOG" << e13 << endl;
        }
        if (abs(e23) > 0.00000000000001)
        {
            cout << "e23 NOT ORTHOG" << e23 << endl;
        }

}

