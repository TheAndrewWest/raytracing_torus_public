/* Structures */

#define DISK_HORIZON_MU 51
#define DISK_STRUCTURE 10001
#define DIM 4 

typedef struct 
{
    double M,a,hair,Mdot, height;
    
	// important for ray-tracing
    int n_horizon_mu;
    double horizon[DISK_HORIZON_MU]; // event horizon as function of mu [M]

    // important for modeling of emission and scattering
    double ISCO; // [M]
    double r1,r2,rmax;
    
    int n_disk_structure;

    double eta;
    double u     [DISK_STRUCTURE][DIM]; // 4-velocity of disk element
    double emui  [DISK_STRUCTURE][DIM][DIM];
    double eimu  [DISK_STRUCTURE][DIM][DIM];
    
    // radial integration

//    double r1      [DISK_SAMPLE]; // lower end of bin
//    double r2      [DISK_SAMPLE]; // upper end of bin

    double r       [DISK_STRUCTURE]; // center of bin
    double r_T     [DISK_STRUCTURE]; // T in bin
    double r_weight[DISK_STRUCTURE]; // weight of bin (proportional of flux from bin)
    double torusHeight;
    
} Disk, *Disk_p;


typedef struct 
{
    /* emission position and angle */
    double r0;
    double iron;  // if the photon ever created an iron line
    double T; // temperature of thermal photon */
    double Ei; // initial energy of the photon in the lamp-post
    double u0PF[DIM]; // Plasma Frame 4-velocity
    double u0BL[DIM]; // Boyer Lindquist 4-velocity
    double E0;
    double E,L,b;
    
    /* tracking variables */
    double xBL[DIM]; // BL 4-coord
    double uBL[DIM]; // BL 4-velocity
    double fBL[DIM]; // polarization 4-vector  
    double pol;

    double xSC[DIM];
    double uSC[DIM];
    
    double uCS[DIM];  // 4-wave vector in coordinate-stationary [CS] reference frame
    double fCS[DIM];  // polarization  in coordinate-stationary [CS] reference frame
    double NuCS,NfCS; // parameters to monitor the accuracy of the ray tracing
    double stokes[3]; // Stokes vector in CS frame.

    double weight; // weight
    int    nScatter; // number of scatterings
    int   timeout;

  double scatter[12][25]; // this assumes that the particle doesn't hit more than 100 times for a lamp-post.  There is a check in the code to make sure it doesn't but this won't necessarily work for the extended corona models

} Photon, *Photon_p;


typedef struct 
{
    /* emission position and angle */
    float r0; // Boyer Lindquist radial coordinate r0 from which the photon was launched. Doubles as height for unscattered lamp-post photons
    float T; // temperature of the disk where the photon was launched in eV */
    float u0PF[DIM]; // Plasma Frame 4-velocity with which the photon was launched
    float E0; // energy at infinity when the photon was launched
    float E; // energy at infinity when the photon arrives; may differ from E0 owing to scatterings
    float Ei; // initial energy of the photon when it was emitted from the lamp-post
    float iron; // 1 if an iron line was created at some point
    
    /* tracking variables */
    float xBL[DIM]; // Boyer Lindquist 4-vector of the final position of the photon
    float uCS[DIM];  // 4-wave vector in coordinate-stationary [CS] reference frame
    float fCS[DIM];  // polarization  in coordinate-stationary [CS] reference frame
    
    float NuCS,NfCS; // parameters to monitor the accuracy of the ray tracing
    float stokes[3]; // Stokes vector in CS frame.
    int nScatter; // number of scatterings
    int timeout;

    float scatter[12][25];

  /* scatter[0-3][nscatter] = uPF[0-3] -> uVector entering the disk
     scatter[4][nscatter] = y[0][1]
     scatter[5-8][nscatter] = u0PF -> uVector leaving the disk
     scatter[9][nscatter] = iron -> 1 if it creates an iron line when scattering */

} PhotonRed, *PhotonRed_p;

/* === === === === === === === === === === === === === === */

void initializePhoton(Photon *gamma_p);
void saveData(std::ofstream& o_file, Photon *gamma_p, PhotonRed *gammaRed_p, int total, int size, int frequency);
void loadConfig(char* nConfig, Disk *disk_p);
void loadConfig2(int nConfig, Disk *disk_p);
void generateSource(Photon *gamma_p, Disk *disk_p, double h, double Ei, int geometry);
void generatePhotonThinDisk(Disk *disk_p, int ir,Photon *res_p, double Ei, int geometry);
//void generatePhotonThickDisk(Disk *disk_p, int ir,Photon *gamma_p, double Ei, int geometry, int runMode, double emu[4][4], double y[10], int runMode);
void generatePhotonThickDisk(Disk *disk_p, int ir,Photon *gamma_p, double Ei, int geometry, int runMode, double emu[4][4], double y[10]);

void trackPhoton(Disk *disk_p,Photon *gamma_p, double accuracy1, double accuracy2,std::ofstream &os,int flag,double runMode, int geometry);
void tab24(double mu, double &Is, double &pol);
void tab25(double *F, double mu0, double phi0, double *I, double mu, double phi);
void CS_emui(const std::vector<double> &metric, double emui[4][4]);

void load_table(double *t_x,int &numbins, char *fname1,int checknum);
double get_value_log(double *t_x,double *t_v,int numbins,double x);
double get_value_lin(double *t_x,double *t_v,int numbins,double x);

double ran1(long *idum);

int linBin(double x1,double x2,int numbins,double x);
int logBin(double x1,double x2,int numbins,double x);
double binLog(double x1,double x2,int numbins,int nbin);
double cot(double x);

void Matrix_SMult(double c,double a1[][3], double a2[][3]);
void Matrix_Add(double a1[][3], double a2[][3], double a3[][3]);
void Matrix_Mult(double a1[][3], double a2[][3], double a3[][3]);
double dot3(double a1[][3], double a2[][3], int aRow, int bCol);
void Matrix_Print(double *F,int n, int l);
void MatrixVector_Mult(double a1[][3], double v[3], double r[3]);
double dot(double *a, double *b);
void normalize(double *v,double *n);
void perp(double *v,double *k,double *p);
double angle(double *aNorm,double *bNorm);
void cross(double *a, double *b, double *c);
double getChi(double q, double u);

#define NMAX 4
void migs (double a[][NMAX],int n,double x[][NMAX]);
void elgs (double a[][NMAX],int n,int indx[NMAX]);

double comptonLow(double En, double th);
double comptonHigh(double En, double th);
double effectiveYield(double theta);




