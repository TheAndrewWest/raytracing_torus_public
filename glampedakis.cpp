#include "glampedakis.h"
#include "metric.h"
#include "xTrackMath.h"

#include <cmath>
#include <iostream>


void GlampedakisMetric::Initialize(double)
{
  ::CalculateMetricElements = &GlampedakisMetric::CalculateMetricElements;
  ::CalculateMetricElements3 = &GlampedakisMetric::CalculateMetricElements3;
  ::CalculateChristoffelSymbols = &GlampedakisMetric::CalculateChristoffelSymbols;
}
  

void GlampedakisMetric::CalculateChristoffelSymbols(double M, double a, double hair, const double *y, std::vector<double> &symbols)
{
  const double a_2 = a*a;
  const double a_3 = a_2*a;
  const double a_4 = a_3*a;
  const double M_2 = M*M;
  const double y1 = y[1];
  const double y1_2 = y1*y1;
  const double y1_3 = y1_2*y1;
  const double y1_4 = y1_3*y1;
  const double y2 = y[2];
  const double sin_y2 = sin(y2);
  const double sin_y2_2 = sin_y2*sin_y2;
  const double sin_y2_3 = sin_y2_2*sin_y2;
  const double sin_y2_4 = sin_y2_3*sin_y2;
  const double sin_y2_m2 = 1/sin_y2_2;
  const double cos_y2 = cos(y2);
  const double cos_y2_2 = cos_y2*cos_y2;
  const double cot_y2 = cos_y2/sin_y2;
  const double _y1_m_2M = y1 - 2*M;
  const double pow2_y1_m_2M = sqr(_y1_m_2M);
  const double pow3_y1_m_2M = pow2_y1_m_2M*_y1_m_2M;
  const double _y1_2_p_a_2cos_y2_2 = y1_2 + a_2*cos_y2_2;
  const double pow2_y1_2_p_a_2cos_y2_2 = sqr(_y1_2_p_a_2cos_y2_2);
  const double log_y1__y1_m_2M = log(y1/(y1 - 2*M));

  symbols[1] = symbols[4] =
    (y1*a*M*sin_y2_2*((4*y1_2*a*M*sin_y2_2)/pow2_y1_2_p_a_2cos_y2_2 - (2*a*M*sin_y2_2)/(y1_2 + a_2*cos_y2_2)))/((y1_2 + a_2*cos_y2_2)*((-4*y1_2*a_2*M_2*sin_y2_4)/pow2_y1_2_p_a_2cos_y2_2 + (-1 + (2*y1*M)/(y1_2 + a_2*cos_y2_2) + (5*hair*(-1 + 3*cos_y2_2)*(-2*M*(-y1 + M)*(-3*y1_2 + 6*y1*M + 2*M_2) + 3*y1_2*pow2_y1_m_2M*log_y1__y1_m_2M))/(16.*pow2_y1_m_2M*M_2))*((5*hair*(-1 + 3*cos_y2_2)*sin_y2_m2*(2*M*(-3*y1_2 - 3*y1*M + 2*M_2) + 3*y1*(y1_2 - 2*M_2)*log_y1__y1_m_2M))/(16.*y1_3*M_2) + sin_y2_2*(y1_2 + a_2 + (2*y1*a_2*M*sin_y2_2)/(y1_2 + a_2*cos_y2_2))))) + (((-4*y1_2*M)/pow2_y1_2_p_a_2cos_y2_2 + (2*M)/(y1_2 + a_2*cos_y2_2) + (5*hair*(-1 + 3*cos_y2_2)*(3*y1*(-(y1/pow2_y1_m_2M) + 1/(y1 - 2*M))*pow3_y1_m_2M - 2*M*(-y1 + M)*(-6*y1 + 6*M) + 2*M*(-3*y1_2 + 6*y1*M + 2*M_2) + 6*y1_2*(y1 - 2*M)*log_y1__y1_m_2M + 6*y1*pow2_y1_m_2M*log_y1__y1_m_2M))/(16.*pow2_y1_m_2M*M_2) - (5*hair*(-1 + 3*cos_y2_2)*(-2*M*(-y1 + M)*(-3*y1_2 + 6*y1*M + 2*M_2) + 3*y1_2*pow2_y1_m_2M*log_y1__y1_m_2M))/(8.*pow3_y1_m_2M*M_2))*((5*hair*(-1 + 3*cos_y2_2)*sin_y2_m2*(2*M*(-3*y1_2 - 3*y1*M + 2*M_2) + 3*y1*(y1_2 - 2*M_2)*log_y1__y1_m_2M))/(16.*y1_3*M_2) + sin_y2_2*(y1_2 + a_2 + (2*y1*a_2*M*sin_y2_2)/(y1_2 + a_2*cos_y2_2))))/(2.*((-4*y1_2*a_2*M_2*sin_y2_4)/pow2_y1_2_p_a_2cos_y2_2 + (-1 + (2*y1*M)/(y1_2 + a_2*cos_y2_2) + (5*hair*(-1 + 3*cos_y2_2)*(-2*M*(-y1 + M)*(-3*y1_2 + 6*y1*M + 2*M_2) + 3*y1_2*pow2_y1_m_2M*log_y1__y1_m_2M))/(16.*pow2_y1_m_2M*M_2))*((5*hair*(-1 + 3*cos_y2_2)*sin_y2_m2*(2*M*(-3*y1_2 - 3*y1*M + 2*M_2) + 3*y1*(y1_2 - 2*M_2)*log_y1__y1_m_2M))/(16.*y1_3*M_2) + sin_y2_2*(y1_2 + a_2 + (2*y1*a_2*M*sin_y2_2)/(y1_2 + a_2*cos_y2_2)))));

  symbols[2] = symbols[8] =
    (y1*a*M*sin_y2_2*((-4*y1*a*M*cos_y2*sin_y2)/(y1_2 + a_2*cos_y2_2) - (4*y1*a_3*M*cos_y2*sin_y2_3)/pow2_y1_2_p_a_2cos_y2_2))/((y1_2 + a_2*cos_y2_2)*((-4*y1_2*a_2*M_2*sin_y2_4)/pow2_y1_2_p_a_2cos_y2_2 + (-1 + (2*y1*M)/(y1_2 + a_2*cos_y2_2) + (5*hair*(-1 + 3*cos_y2_2)*(-2*M*(-y1 + M)*(-3*y1_2 + 6*y1*M + 2*M_2) + 3*y1_2*pow2_y1_m_2M*log_y1__y1_m_2M))/(16.*pow2_y1_m_2M*M_2))*((5*hair*(-1 + 3*cos_y2_2)*sin_y2_m2*(2*M*(-3*y1_2 - 3*y1*M + 2*M_2) + 3*y1*(y1_2 - 2*M_2)*log_y1__y1_m_2M))/(16.*y1_3*M_2) + sin_y2_2*(y1_2 + a_2 + (2*y1*a_2*M*sin_y2_2)/(y1_2 + a_2*cos_y2_2))))) + (((4*y1*a_2*M*cos_y2*sin_y2)/pow2_y1_2_p_a_2cos_y2_2 - (15*hair*cos_y2*(-2*M*(-y1 + M)*(-3*y1_2 + 6*y1*M + 2*M_2) + 3*y1_2*pow2_y1_m_2M*log_y1__y1_m_2M)*sin_y2)/(8.*pow2_y1_m_2M*M_2))*((5*hair*(-1 + 3*cos_y2_2)*sin_y2_m2*(2*M*(-3*y1_2 - 3*y1*M + 2*M_2) + 3*y1*(y1_2 - 2*M_2)*log_y1__y1_m_2M))/(16.*y1_3*M_2) + sin_y2_2*(y1_2 + a_2 + (2*y1*a_2*M*sin_y2_2)/(y1_2 + a_2*cos_y2_2))))/(2.*((-4*y1_2*a_2*M_2*sin_y2_4)/pow2_y1_2_p_a_2cos_y2_2 + (-1 + (2*y1*M)/(y1_2 + a_2*cos_y2_2) + (5*hair*(-1 + 3*cos_y2_2)*(-2*M*(-y1 + M)*(-3*y1_2 + 6*y1*M + 2*M_2) + 3*y1_2*pow2_y1_m_2M*log_y1__y1_m_2M))/(16.*pow2_y1_m_2M*M_2))*((5*hair*(-1 + 3*cos_y2_2)*sin_y2_m2*(2*M*(-3*y1_2 - 3*y1*M + 2*M_2) + 3*y1*(y1_2 - 2*M_2)*log_y1__y1_m_2M))/(16.*y1_3*M_2) + sin_y2_2*(y1_2 + a_2 + (2*y1*a_2*M*sin_y2_2)/(y1_2 + a_2*cos_y2_2)))));

  symbols[7] = symbols[13] =
    (y1*a*M*sin_y2_2*((5*hair*(-1 + 3*cos_y2_2)*sin_y2_m2*(2*(-6*y1 - 3*M)*M + 3*(-(y1/pow2_y1_m_2M) + 1/(y1 - 2*M))*(y1 - 2*M)*(y1_2 - 2*M_2) + 6*y1_2*log_y1__y1_m_2M + 3*(y1_2 - 2*M_2)*log_y1__y1_m_2M))/(16.*y1_3*M_2) - (15*hair*(-1 + 3*cos_y2_2)*sin_y2_m2*(2*M*(-3*y1_2 - 3*y1*M + 2*M_2) + 3*y1*(y1_2 - 2*M_2)*log_y1__y1_m_2M))/(16.*y1_4*M_2) + sin_y2_2*(2*y1 - (4*y1_2*a_2*M*sin_y2_2)/pow2_y1_2_p_a_2cos_y2_2 + (2*a_2*M*sin_y2_2)/(y1_2 + a_2*cos_y2_2))))/((y1_2 + a_2*cos_y2_2)*((-4*y1_2*a_2*M_2*sin_y2_4)/pow2_y1_2_p_a_2cos_y2_2 + (-1 + (2*y1*M)/(y1_2 + a_2*cos_y2_2) + (5*hair*(-1 + 3*cos_y2_2)*(-2*M*(-y1 + M)*(-3*y1_2 + 6*y1*M + 2*M_2) + 3*y1_2*pow2_y1_m_2M*log_y1__y1_m_2M))/(16.*pow2_y1_m_2M*M_2))*((5*hair*(-1 + 3*cos_y2_2)*sin_y2_m2*(2*M*(-3*y1_2 - 3*y1*M + 2*M_2) + 3*y1*(y1_2 - 2*M_2)*log_y1__y1_m_2M))/(16.*y1_3*M_2) + sin_y2_2*(y1_2 + a_2 + (2*y1*a_2*M*sin_y2_2)/(y1_2 + a_2*cos_y2_2))))) + (((4*y1_2*a*M*sin_y2_2)/pow2_y1_2_p_a_2cos_y2_2 - (2*a*M*sin_y2_2)/(y1_2 + a_2*cos_y2_2))*((5*hair*(-1 + 3*cos_y2_2)*sin_y2_m2*(2*M*(-3*y1_2 - 3*y1*M + 2*M_2) + 3*y1*(y1_2 - 2*M_2)*log_y1__y1_m_2M))/(16.*y1_3*M_2) + sin_y2_2*(y1_2 + a_2 + (2*y1*a_2*M*sin_y2_2)/(y1_2 + a_2*cos_y2_2))))/(2.*((-4*y1_2*a_2*M_2*sin_y2_4)/pow2_y1_2_p_a_2cos_y2_2 + (-1 + (2*y1*M)/(y1_2 + a_2*cos_y2_2) + (5*hair*(-1 + 3*cos_y2_2)*(-2*M*(-y1 + M)*(-3*y1_2 + 6*y1*M + 2*M_2) + 3*y1_2*pow2_y1_m_2M*log_y1__y1_m_2M))/(16.*pow2_y1_m_2M*M_2))*((5*hair*(-1 + 3*cos_y2_2)*sin_y2_m2*(2*M*(-3*y1_2 - 3*y1*M + 2*M_2) + 3*y1*(y1_2 - 2*M_2)*log_y1__y1_m_2M))/(16.*y1_3*M_2) + sin_y2_2*(y1_2 + a_2 + (2*y1*a_2*M*sin_y2_2)/(y1_2 + a_2*cos_y2_2)))));

  symbols[11] = symbols[14] =
    (((-4*y1*a*M*cos_y2*sin_y2)/(y1_2 + a_2*cos_y2_2) - (4*y1*a_3*M*cos_y2*sin_y2_3)/pow2_y1_2_p_a_2cos_y2_2)*((5*hair*(-1 + 3*cos_y2_2)*sin_y2_m2*(2*M*(-3*y1_2 - 3*y1*M + 2*M_2) + 3*y1*(y1_2 - 2*M_2)*log_y1__y1_m_2M))/(16.*y1_3*M_2) + sin_y2_2*(y1_2 + a_2 + (2*y1*a_2*M*sin_y2_2)/(y1_2 + a_2*cos_y2_2))))/(2.*((-4*y1_2*a_2*M_2*sin_y2_4)/pow2_y1_2_p_a_2cos_y2_2 + (-1 + (2*y1*M)/(y1_2 + a_2*cos_y2_2) + (5*hair*(-1 + 3*cos_y2_2)*(-2*M*(-y1 + M)*(-3*y1_2 + 6*y1*M + 2*M_2) + 3*y1_2*pow2_y1_m_2M*log_y1__y1_m_2M))/(16.*pow2_y1_m_2M*M_2))*((5*hair*(-1 + 3*cos_y2_2)*sin_y2_m2*(2*M*(-3*y1_2 - 3*y1*M + 2*M_2) + 3*y1*(y1_2 - 2*M_2)*log_y1__y1_m_2M))/(16.*y1_3*M_2) + sin_y2_2*(y1_2 + a_2 + (2*y1*a_2*M*sin_y2_2)/(y1_2 + a_2*cos_y2_2))))) + (y1*a*M*sin_y2_2*((-15*hair*cot_y2*(2*M*(-3*y1_2 - 3*y1*M + 2*M_2) + 3*y1*(y1_2 - 2*M_2)*log_y1__y1_m_2M))/(8.*y1_3*M_2) - (5*hair*(-1 + 3*cos_y2_2)*cot_y2*sin_y2_m2*(2*M*(-3*y1_2 - 3*y1*M + 2*M_2) + 3*y1*(y1_2 - 2*M_2)*log_y1__y1_m_2M))/(8.*y1_3*M_2) + 2*cos_y2*sin_y2*(y1_2 + a_2 + (2*y1*a_2*M*sin_y2_2)/(y1_2 + a_2*cos_y2_2)) + sin_y2_2*((4*y1*a_2*M*cos_y2*sin_y2)/(y1_2 + a_2*cos_y2_2) + (4*y1*a_4*M*cos_y2*sin_y2_3)/pow2_y1_2_p_a_2cos_y2_2)))/((y1_2 + a_2*cos_y2_2)*((-4*y1_2*a_2*M_2*sin_y2_4)/pow2_y1_2_p_a_2cos_y2_2 + (-1 + (2*y1*M)/(y1_2 + a_2*cos_y2_2) + (5*hair*(-1 + 3*cos_y2_2)*(-2*M*(-y1 + M)*(-3*y1_2 + 6*y1*M + 2*M_2) + 3*y1_2*pow2_y1_m_2M*log_y1__y1_m_2M))/(16.*pow2_y1_m_2M*M_2))*((5*hair*(-1 + 3*cos_y2_2)*sin_y2_m2*(2*M*(-3*y1_2 - 3*y1*M + 2*M_2) + 3*y1*(y1_2 - 2*M_2)*log_y1__y1_m_2M))/(16.*y1_3*M_2) + sin_y2_2*(y1_2 + a_2 + (2*y1*a_2*M*sin_y2_2)/(y1_2 + a_2*cos_y2_2)))));

  symbols[16] = ((4*y1_2*M)/pow2_y1_2_p_a_2cos_y2_2 - (2*M)/(y1_2 + a_2*cos_y2_2) - (5*hair*(-1 + 3*cos_y2_2)*(3*y1*(-(y1/pow2_y1_m_2M) + 1/(y1 - 2*M))*pow3_y1_m_2M - 2*M*(-y1 + M)*(-6*y1 + 6*M) + 2*M*(-3*y1_2 + 6*y1*M + 2*M_2) + 6*y1_2*(y1 - 2*M)*log_y1__y1_m_2M + 6*y1*pow2_y1_m_2M*log_y1__y1_m_2M))/(16.*pow2_y1_m_2M*M_2) + (5*hair*(-1 + 3*cos_y2_2)*(-2*M*(-y1 + M)*(-3*y1_2 + 6*y1*M + 2*M_2) + 3*y1_2*pow2_y1_m_2M*log_y1__y1_m_2M))/(8.*pow3_y1_m_2M*M_2))/(2.*((y1_2 + a_2*cos_y2_2)/(y1_2 + a_2 - 2*y1*M) + (5*hair*(-1 + 3*cos_y2_2)*(-2*M*(-y1 + M)*(-3*y1_2 + 6*y1*M + 2*M_2) + 3*y1_2*pow2_y1_m_2M*log_y1__y1_m_2M))/(16.*y1_2*M_2)));

  symbols[19] = symbols[28] =
    ((-4*y1_2*a*M*sin_y2_2)/pow2_y1_2_p_a_2cos_y2_2 + (2*a*M*sin_y2_2)/(y1_2 + a_2*cos_y2_2))/(2.*((y1_2 + a_2*cos_y2_2)/(y1_2 + a_2 - 2*y1*M) + (5*hair*(-1 + 3*cos_y2_2)*(-2*M*(-y1 + M)*(-3*y1_2 + 6*y1*M + 2*M_2) + 3*y1_2*pow2_y1_m_2M*log_y1__y1_m_2M))/(16.*y1_2*M_2)));

  symbols[21] = ((2*y1)/(y1_2 + a_2 - 2*y1*M) - ((2*y1 - 2*M)*(y1_2 + a_2*cos_y2_2))/pow(y1_2 + a_2 - 2*y1*M,2) + (5*hair*(-1 + 3*cos_y2_2)*(3*y1*(-(y1/pow2_y1_m_2M) + 1/(y1 - 2*M))*pow3_y1_m_2M - 2*M*(-y1 + M)*(-6*y1 + 6*M) + 2*M*(-3*y1_2 + 6*y1*M + 2*M_2) + 6*y1_2*(y1 - 2*M)*log_y1__y1_m_2M + 6*y1*pow2_y1_m_2M*log_y1__y1_m_2M))/(16.*y1_2*M_2) - (5*hair*(-1 + 3*cos_y2_2)*(-2*M*(-y1 + M)*(-3*y1_2 + 6*y1*M + 2*M_2) + 3*y1_2*pow2_y1_m_2M*log_y1__y1_m_2M))/(8.*y1_3*M_2))/(2.*((y1_2 + a_2*cos_y2_2)/(y1_2 + a_2 - 2*y1*M) + (5*hair*(-1 + 3*cos_y2_2)*(-2*M*(-y1 + M)*(-3*y1_2 + 6*y1*M + 2*M_2) + 3*y1_2*pow2_y1_m_2M*log_y1__y1_m_2M))/(16.*y1_2*M_2)));

  symbols[22] = symbols[25] =
    ((-2*a_2*cos_y2*sin_y2)/(y1_2 + a_2 - 2*y1*M) - (15*hair*cos_y2*(-2*M*(-y1 + M)*(-3*y1_2 + 6*y1*M + 2*M_2) + 3*y1_2*pow2_y1_m_2M*log_y1__y1_m_2M)*sin_y2)/(8.*y1_2*M_2))/(2.*((y1_2 + a_2*cos_y2_2)/(y1_2 + a_2 - 2*y1*M) + (5*hair*(-1 + 3*cos_y2_2)*(-2*M*(-y1 + M)*(-3*y1_2 + 6*y1*M + 2*M_2) + 3*y1_2*pow2_y1_m_2M*log_y1__y1_m_2M))/(16.*y1_2*M_2)));

  symbols[26] = (-2*y1 - (5*hair*(-1 + 3*cos_y2_2)*(2*(-6*y1 - 3*M)*M + 3*(-(y1/pow2_y1_m_2M) + 1/(y1 - 2*M))*(y1 - 2*M)*(y1_2 - 2*M_2) + 6*y1_2*log_y1__y1_m_2M + 3*(y1_2 - 2*M_2)*log_y1__y1_m_2M))/(16.*y1_3*M_2) + (15*hair*(-1 + 3*cos_y2_2)*(2*M*(-3*y1_2 - 3*y1*M + 2*M_2) + 3*y1*(y1_2 - 2*M_2)*log_y1__y1_m_2M))/(16.*y1_4*M_2))/(2.*((y1_2 + a_2*cos_y2_2)/(y1_2 + a_2 - 2*y1*M) + (5*hair*(-1 + 3*cos_y2_2)*(-2*M*(-y1 + M)*(-3*y1_2 + 6*y1*M + 2*M_2) + 3*y1_2*pow2_y1_m_2M*log_y1__y1_m_2M))/(16.*y1_2*M_2)));

  symbols[31] = ((-5*hair*(-1 + 3*cos_y2_2)*sin_y2_m2*(2*(-6*y1 - 3*M)*M + 3*(-(y1/pow2_y1_m_2M) + 1/(y1 - 2*M))*(y1 - 2*M)*(y1_2 - 2*M_2) + 6*y1_2*log_y1__y1_m_2M + 3*(y1_2 - 2*M_2)*log_y1__y1_m_2M))/(16.*y1_3*M_2) + (15*hair*(-1 + 3*cos_y2_2)*sin_y2_m2*(2*M*(-3*y1_2 - 3*y1*M + 2*M_2) + 3*y1*(y1_2 - 2*M_2)*log_y1__y1_m_2M))/(16.*y1_4*M_2) - sin_y2_2*(2*y1 - (4*y1_2*a_2*M*sin_y2_2)/pow2_y1_2_p_a_2cos_y2_2 + (2*a_2*M*sin_y2_2)/(y1_2 + a_2*cos_y2_2)))/(2.*((y1_2 + a_2*cos_y2_2)/(y1_2 + a_2 - 2*y1*M) + (5*hair*(-1 + 3*cos_y2_2)*(-2*M*(-y1 + M)*(-3*y1_2 + 6*y1*M + 2*M_2) + 3*y1_2*pow2_y1_m_2M*log_y1__y1_m_2M))/(16.*y1_2*M_2)));

  symbols[32] = ((-4*y1*a_2*M*cos_y2*sin_y2)/pow2_y1_2_p_a_2cos_y2_2 + (15*hair*cos_y2*(-2*M*(-y1 + M)*(-3*y1_2 + 6*y1*M + 2*M_2) + 3*y1_2*pow2_y1_m_2M*log_y1__y1_m_2M)*sin_y2)/(8.*pow2_y1_m_2M*M_2))/(2.*(y1_2 + a_2*cos_y2_2 + (5*hair*(-1 + 3*cos_y2_2)*(2*M*(-3*y1_2 - 3*y1*M + 2*M_2) + 3*y1*(y1_2 - 2*M_2)*log_y1__y1_m_2M))/(16.*y1_3*M_2)));

  symbols[35] = symbols[44] =
    ((4*y1*a*M*cos_y2*sin_y2)/(y1_2 + a_2*cos_y2_2) + (4*y1*a_3*M*cos_y2*sin_y2_3)/pow2_y1_2_p_a_2cos_y2_2)/(2.*(y1_2 + a_2*cos_y2_2 + (5*hair*(-1 + 3*cos_y2_2)*(2*M*(-3*y1_2 - 3*y1*M + 2*M_2) + 3*y1*(y1_2 - 2*M_2)*log_y1__y1_m_2M))/(16.*y1_3*M_2)));

  symbols[37] = ((2*a_2*cos_y2*sin_y2)/(y1_2 + a_2 - 2*y1*M) + (15*hair*cos_y2*(-2*M*(-y1 + M)*(-3*y1_2 + 6*y1*M + 2*M_2) + 3*y1_2*pow2_y1_m_2M*log_y1__y1_m_2M)*sin_y2)/(8.*y1_2*M_2))/(2.*(y1_2 + a_2*cos_y2_2 + (5*hair*(-1 + 3*cos_y2_2)*(2*M*(-3*y1_2 - 3*y1*M + 2*M_2) + 3*y1*(y1_2 - 2*M_2)*log_y1__y1_m_2M))/(16.*y1_3*M_2)));

  symbols[38] = symbols[41] =
    (2*y1 + (5*hair*(-1 + 3*cos_y2_2)*(2*(-6*y1 - 3*M)*M + 3*(-(y1/pow2_y1_m_2M) + 1/(y1 - 2*M))*(y1 - 2*M)*(y1_2 - 2*M_2) + 6*y1_2*log_y1__y1_m_2M + 3*(y1_2 - 2*M_2)*log_y1__y1_m_2M))/(16.*y1_3*M_2) - (15*hair*(-1 + 3*cos_y2_2)*(2*M*(-3*y1_2 - 3*y1*M + 2*M_2) + 3*y1*(y1_2 - 2*M_2)*log_y1__y1_m_2M))/(16.*y1_4*M_2))/(2.*(y1_2 + a_2*cos_y2_2 + (5*hair*(-1 + 3*cos_y2_2)*(2*M*(-3*y1_2 - 3*y1*M + 2*M_2) + 3*y1*(y1_2 - 2*M_2)*log_y1__y1_m_2M))/(16.*y1_3*M_2)));

  symbols[42] = (-2*a_2*cos_y2*sin_y2 - (15*hair*cos_y2*(2*M*(-3*y1_2 - 3*y1*M + 2*M_2) + 3*y1*(y1_2 - 2*M_2)*log_y1__y1_m_2M)*sin_y2)/(8.*y1_3*M_2))/(2.*(y1_2 + a_2*cos_y2_2 + (5*hair*(-1 + 3*cos_y2_2)*(2*M*(-3*y1_2 - 3*y1*M + 2*M_2) + 3*y1*(y1_2 - 2*M_2)*log_y1__y1_m_2M))/(16.*y1_3*M_2)));

  symbols[47] = ((15*hair*cot_y2*(2*M*(-3*y1_2 - 3*y1*M + 2*M_2) + 3*y1*(y1_2 - 2*M_2)*log_y1__y1_m_2M))/(8.*y1_3*M_2) + (5*hair*(-1 + 3*cos_y2_2)*cot_y2*sin_y2_m2*(2*M*(-3*y1_2 - 3*y1*M + 2*M_2) + 3*y1*(y1_2 - 2*M_2)*log_y1__y1_m_2M))/(8.*y1_3*M_2) - 2*cos_y2*sin_y2*(y1_2 + a_2 + (2*y1*a_2*M*sin_y2_2)/(y1_2 + a_2*cos_y2_2)) - sin_y2_2*((4*y1*a_2*M*cos_y2*sin_y2)/(y1_2 + a_2*cos_y2_2) + (4*y1*a_4*M*cos_y2*sin_y2_3)/pow2_y1_2_p_a_2cos_y2_2))/(2.*(y1_2 + a_2*cos_y2_2 + (5*hair*(-1 + 3*cos_y2_2)*(2*M*(-3*y1_2 - 3*y1*M + 2*M_2) + 3*y1*(y1_2 - 2*M_2)*log_y1__y1_m_2M))/(16.*y1_3*M_2)));

  symbols[49] = symbols[52] =
    (y1*a*M*((-4*y1_2*M)/pow2_y1_2_p_a_2cos_y2_2 + (2*M)/(y1_2 + a_2*cos_y2_2) + (5*hair*(-1 + 3*cos_y2_2)*(3*y1*(-(y1/pow2_y1_m_2M) + 1/(y1 - 2*M))*pow3_y1_m_2M - 2*M*(-y1 + M)*(-6*y1 + 6*M) + 2*M*(-3*y1_2 + 6*y1*M + 2*M_2) + 6*y1_2*(y1 - 2*M)*log_y1__y1_m_2M + 6*y1*pow2_y1_m_2M*log_y1__y1_m_2M))/(16.*pow2_y1_m_2M*M_2) - (5*hair*(-1 + 3*cos_y2_2)*(-2*M*(-y1 + M)*(-3*y1_2 + 6*y1*M + 2*M_2) + 3*y1_2*pow2_y1_m_2M*log_y1__y1_m_2M))/(8.*pow3_y1_m_2M*M_2))*sin_y2_2)/((y1_2 + a_2*cos_y2_2)*((-4*y1_2*a_2*M_2*sin_y2_4)/pow2_y1_2_p_a_2cos_y2_2 + (-1 + (2*y1*M)/(y1_2 + a_2*cos_y2_2) + (5*hair*(-1 + 3*cos_y2_2)*(-2*M*(-y1 + M)*(-3*y1_2 + 6*y1*M + 2*M_2) + 3*y1_2*pow2_y1_m_2M*log_y1__y1_m_2M))/(16.*pow2_y1_m_2M*M_2))*((5*hair*(-1 + 3*cos_y2_2)*sin_y2_m2*(2*M*(-3*y1_2 - 3*y1*M + 2*M_2) + 3*y1*(y1_2 - 2*M_2)*log_y1__y1_m_2M))/(16.*y1_3*M_2) + sin_y2_2*(y1_2 + a_2 + (2*y1*a_2*M*sin_y2_2)/(y1_2 + a_2*cos_y2_2))))) + ((-1 + (2*y1*M)/(y1_2 + a_2*cos_y2_2) + (5*hair*(-1 + 3*cos_y2_2)*(-2*M*(-y1 + M)*(-3*y1_2 + 6*y1*M + 2*M_2) + 3*y1_2*pow2_y1_m_2M*log_y1__y1_m_2M))/(16.*pow2_y1_m_2M*M_2))*((4*y1_2*a*M*sin_y2_2)/pow2_y1_2_p_a_2cos_y2_2 - (2*a*M*sin_y2_2)/(y1_2 + a_2*cos_y2_2)))/(2.*((-4*y1_2*a_2*M_2*sin_y2_4)/pow2_y1_2_p_a_2cos_y2_2 + (-1 + (2*y1*M)/(y1_2 + a_2*cos_y2_2) + (5*hair*(-1 + 3*cos_y2_2)*(-2*M*(-y1 + M)*(-3*y1_2 + 6*y1*M + 2*M_2) + 3*y1_2*pow2_y1_m_2M*log_y1__y1_m_2M))/(16.*pow2_y1_m_2M*M_2))*((5*hair*(-1 + 3*cos_y2_2)*sin_y2_m2*(2*M*(-3*y1_2 - 3*y1*M + 2*M_2) + 3*y1*(y1_2 - 2*M_2)*log_y1__y1_m_2M))/(16.*y1_3*M_2) + sin_y2_2*(y1_2 + a_2 + (2*y1*a_2*M*sin_y2_2)/(y1_2 + a_2*cos_y2_2)))));

  symbols[50] = symbols[56] =
    (y1*a*M*sin_y2_2*((4*y1*a_2*M*cos_y2*sin_y2)/pow2_y1_2_p_a_2cos_y2_2 - (15*hair*cos_y2*(-2*M*(-y1 + M)*(-3*y1_2 + 6*y1*M + 2*M_2) + 3*y1_2*pow2_y1_m_2M*log_y1__y1_m_2M)*sin_y2)/(8.*pow2_y1_m_2M*M_2)))/((y1_2 + a_2*cos_y2_2)*((-4*y1_2*a_2*M_2*sin_y2_4)/pow2_y1_2_p_a_2cos_y2_2 + (-1 + (2*y1*M)/(y1_2 + a_2*cos_y2_2) + (5*hair*(-1 + 3*cos_y2_2)*(-2*M*(-y1 + M)*(-3*y1_2 + 6*y1*M + 2*M_2) + 3*y1_2*pow2_y1_m_2M*log_y1__y1_m_2M))/(16.*pow2_y1_m_2M*M_2))*((5*hair*(-1 + 3*cos_y2_2)*sin_y2_m2*(2*M*(-3*y1_2 - 3*y1*M + 2*M_2) + 3*y1*(y1_2 - 2*M_2)*log_y1__y1_m_2M))/(16.*y1_3*M_2) + sin_y2_2*(y1_2 + a_2 + (2*y1*a_2*M*sin_y2_2)/(y1_2 + a_2*cos_y2_2))))) + ((-1 + (2*y1*M)/(y1_2 + a_2*cos_y2_2) + (5*hair*(-1 + 3*cos_y2_2)*(-2*M*(-y1 + M)*(-3*y1_2 + 6*y1*M + 2*M_2) + 3*y1_2*pow2_y1_m_2M*log_y1__y1_m_2M))/(16.*pow2_y1_m_2M*M_2))*((-4*y1*a*M*cos_y2*sin_y2)/(y1_2 + a_2*cos_y2_2) - (4*y1*a_3*M*cos_y2*sin_y2_3)/pow2_y1_2_p_a_2cos_y2_2))/(2.*((-4*y1_2*a_2*M_2*sin_y2_4)/pow2_y1_2_p_a_2cos_y2_2 + (-1 + (2*y1*M)/(y1_2 + a_2*cos_y2_2) + (5*hair*(-1 + 3*cos_y2_2)*(-2*M*(-y1 + M)*(-3*y1_2 + 6*y1*M + 2*M_2) + 3*y1_2*pow2_y1_m_2M*log_y1__y1_m_2M))/(16.*pow2_y1_m_2M*M_2))*((5*hair*(-1 + 3*cos_y2_2)*sin_y2_m2*(2*M*(-3*y1_2 - 3*y1*M + 2*M_2) + 3*y1*(y1_2 - 2*M_2)*log_y1__y1_m_2M))/(16.*y1_3*M_2) + sin_y2_2*(y1_2 + a_2 + (2*y1*a_2*M*sin_y2_2)/(y1_2 + a_2*cos_y2_2)))));

  symbols[55] = symbols[61] =
    (y1*a*M*sin_y2_2*((4*y1_2*a*M*sin_y2_2)/pow2_y1_2_p_a_2cos_y2_2 - (2*a*M*sin_y2_2)/(y1_2 + a_2*cos_y2_2)))/((y1_2 + a_2*cos_y2_2)*((-4*y1_2*a_2*M_2*sin_y2_4)/pow2_y1_2_p_a_2cos_y2_2 + (-1 + (2*y1*M)/(y1_2 + a_2*cos_y2_2) + (5*hair*(-1 + 3*cos_y2_2)*(-2*M*(-y1 + M)*(-3*y1_2 + 6*y1*M + 2*M_2) + 3*y1_2*pow2_y1_m_2M*log_y1__y1_m_2M))/(16.*pow2_y1_m_2M*M_2))*((5*hair*(-1 + 3*cos_y2_2)*sin_y2_m2*(2*M*(-3*y1_2 - 3*y1*M + 2*M_2) + 3*y1*(y1_2 - 2*M_2)*log_y1__y1_m_2M))/(16.*y1_3*M_2) + sin_y2_2*(y1_2 + a_2 + (2*y1*a_2*M*sin_y2_2)/(y1_2 + a_2*cos_y2_2))))) + ((-1 + (2*y1*M)/(y1_2 + a_2*cos_y2_2) + (5*hair*(-1 + 3*cos_y2_2)*(-2*M*(-y1 + M)*(-3*y1_2 + 6*y1*M + 2*M_2) + 3*y1_2*pow2_y1_m_2M*log_y1__y1_m_2M))/(16.*pow2_y1_m_2M*M_2))*((5*hair*(-1 + 3*cos_y2_2)*sin_y2_m2*(2*(-6*y1 - 3*M)*M + 3*(-(y1/pow2_y1_m_2M) + 1/(y1 - 2*M))*(y1 - 2*M)*(y1_2 - 2*M_2) + 6*y1_2*log_y1__y1_m_2M + 3*(y1_2 - 2*M_2)*log_y1__y1_m_2M))/(16.*y1_3*M_2) - (15*hair*(-1 + 3*cos_y2_2)*sin_y2_m2*(2*M*(-3*y1_2 - 3*y1*M + 2*M_2) + 3*y1*(y1_2 - 2*M_2)*log_y1__y1_m_2M))/(16.*y1_4*M_2) + sin_y2_2*(2*y1 - (4*y1_2*a_2*M*sin_y2_2)/pow2_y1_2_p_a_2cos_y2_2 + (2*a_2*M*sin_y2_2)/(y1_2 + a_2*cos_y2_2))))/(2.*((-4*y1_2*a_2*M_2*sin_y2_4)/pow2_y1_2_p_a_2cos_y2_2 + (-1 + (2*y1*M)/(y1_2 + a_2*cos_y2_2) + (5*hair*(-1 + 3*cos_y2_2)*(-2*M*(-y1 + M)*(-3*y1_2 + 6*y1*M + 2*M_2) + 3*y1_2*pow2_y1_m_2M*log_y1__y1_m_2M))/(16.*pow2_y1_m_2M*M_2))*((5*hair*(-1 + 3*cos_y2_2)*sin_y2_m2*(2*M*(-3*y1_2 - 3*y1*M + 2*M_2) + 3*y1*(y1_2 - 2*M_2)*log_y1__y1_m_2M))/(16.*y1_3*M_2) + sin_y2_2*(y1_2 + a_2 + (2*y1*a_2*M*sin_y2_2)/(y1_2 + a_2*cos_y2_2)))));

  symbols[59] = symbols[62] =
    (y1*a*M*sin_y2_2*((-4*y1*a*M*cos_y2*sin_y2)/(y1_2 + a_2*cos_y2_2) - (4*y1*a_3*M*cos_y2*sin_y2_3)/pow2_y1_2_p_a_2cos_y2_2))/((y1_2 + a_2*cos_y2_2)*((-4*y1_2*a_2*M_2*sin_y2_4)/pow2_y1_2_p_a_2cos_y2_2 + (-1 + (2*y1*M)/(y1_2 + a_2*cos_y2_2) + (5*hair*(-1 + 3*cos_y2_2)*(-2*M*(-y1 + M)*(-3*y1_2 + 6*y1*M + 2*M_2) + 3*y1_2*pow2_y1_m_2M*log_y1__y1_m_2M))/(16.*pow2_y1_m_2M*M_2))*((5*hair*(-1 + 3*cos_y2_2)*sin_y2_m2*(2*M*(-3*y1_2 - 3*y1*M + 2*M_2) + 3*y1*(y1_2 - 2*M_2)*log_y1__y1_m_2M))/(16.*y1_3*M_2) + sin_y2_2*(y1_2 + a_2 + (2*y1*a_2*M*sin_y2_2)/(y1_2 + a_2*cos_y2_2))))) + ((-1 + (2*y1*M)/(y1_2 + a_2*cos_y2_2) + (5*hair*(-1 + 3*cos_y2_2)*(-2*M*(-y1 + M)*(-3*y1_2 + 6*y1*M + 2*M_2) + 3*y1_2*pow2_y1_m_2M*log_y1__y1_m_2M))/(16.*pow2_y1_m_2M*M_2))*((-15*hair*cot_y2*(2*M*(-3*y1_2 - 3*y1*M + 2*M_2) + 3*y1*(y1_2 - 2*M_2)*log_y1__y1_m_2M))/(8.*y1_3*M_2) - (5*hair*(-1 + 3*cos_y2_2)*cot_y2*sin_y2_m2*(2*M*(-3*y1_2 - 3*y1*M + 2*M_2) + 3*y1*(y1_2 - 2*M_2)*log_y1__y1_m_2M))/(8.*y1_3*M_2) + 2*cos_y2*sin_y2*(y1_2 + a_2 + (2*y1*a_2*M*sin_y2_2)/(y1_2 + a_2*cos_y2_2)) + sin_y2_2*((4*y1*a_2*M*cos_y2*sin_y2)/(y1_2 + a_2*cos_y2_2) + (4*y1*a_4*M*cos_y2*sin_y2_3)/pow2_y1_2_p_a_2cos_y2_2)))/(2.*((-4*y1_2*a_2*M_2*sin_y2_4)/pow2_y1_2_p_a_2cos_y2_2 + (-1 + (2*y1*M)/(y1_2 + a_2*cos_y2_2) + (5*hair*(-1 + 3*cos_y2_2)*(-2*M*(-y1 + M)*(-3*y1_2 + 6*y1*M + 2*M_2) + 3*y1_2*pow2_y1_m_2M*log_y1__y1_m_2M))/(16.*pow2_y1_m_2M*M_2))*((5*hair*(-1 + 3*cos_y2_2)*sin_y2_m2*(2*M*(-3*y1_2 - 3*y1*M + 2*M_2) + 3*y1*(y1_2 - 2*M_2)*log_y1__y1_m_2M))/(16.*y1_3*M_2) + sin_y2_2*(y1_2 + a_2 + (2*y1*a_2*M*sin_y2_2)/(y1_2 + a_2*cos_y2_2)))));
}


void GlampedakisMetric::CalculateMetricElements(double M, double a, double hair, const double *y, std::vector<double> &metric)
{
  const double a_2 = sqr(a);
  const double y1 = y[1];
  const double y1_2 = y1*y1;
  const double y1_3 = y1_2 * y1;
  const double M_2 = M*M;
  const double cos_y2_2 = sqr(cos(y[2]));
  const double sin_y2_2 = 1 - cos_y2_2;
  const double sin_y2_m2 = 1/sin_y2_2;
  const double log_y1__y1_m_2M = log(y1/(y1 - 2*M));
  
  metric[0] = -1 + (2*y1*M)/(y1_2 + a_2*cos_y2_2) + (5*hair*(-1 + 3*cos_y2_2)*(-2*M*(-y1 + M)*(-3*y1_2 + 6*y1*M + 2*M_2) + 3*y1_2*pow(y1 - 2*M,2)*log_y1__y1_m_2M))/(16.*pow(y1 - 2*M,2)*M_2);
  metric[3] = (-2*y1*a*M*sin_y2_2)/(y1_2 + a_2*cos_y2_2);
  metric[5] = (y1_2 + a_2*cos_y2_2)/(y1_2 + a_2 - 2*y1*M) + (5*hair*(-1 + 3*cos_y2_2)*(-2*M*(-y1 + M)*(-3*y1_2 + 6*y1*M + 2*M_2) + 3*y1_2*pow(y1 - 2*M,2)*log_y1__y1_m_2M))/(16.*y1_2*M_2);
  metric[10] = y1_2 + a_2*cos_y2_2 + (5*hair*(-1 + 3*cos_y2_2)*(2*M*(-3*y1_2 - 3*y1*M + 2*M_2) + 3*y1*(y1_2 - 2*M_2)*log_y1__y1_m_2M))/(16.*y1_3*M_2);
  metric[12] = (-2*y1*a*M*sin_y2_2)/(y1_2 + a_2*cos_y2_2);
  metric[15] = (5*hair*(-1 + 3*cos_y2_2)*sin_y2_m2*(2*M*(-3*y1_2 - 3*y1*M + 2*M_2) + 3*y1*(y1_2 - 2*M_2)*log_y1__y1_m_2M))/(16.*y1_3*M_2) + sin_y2_2*(y1_2 + a_2 + (2*y1*a_2*M*sin_y2_2)/(y1_2 + a_2*cos_y2_2));
}


void GlampedakisMetric::CalculateMetricElements3(double M, double a, double hair, const double *y, std::vector<double> &metric)
{
  const double a_2 = sqr(a);
  const double y1 = y[1];
  const double y1_2 = y1*y1;
  const double y1_3 = y1_2 * y1;
  const double M_2 = M*M;
  const double cos_y2_2 = sqr(cos(y[2]));
  const double sin_y2_2 = 1 - cos_y2_2;
  const double sin_y2_m2 = 1/sin_y2_2;
  const double log_y1__y1_m_2M = log(y1/(y1 - 2*M));

  metric[0] = -1 + (2*y1*M)/(y1_2 + a_2*cos_y2_2) + (5*hair*(-1 + 3*cos_y2_2)*(-2*M*(-y1 + M)*(-3*y1_2 + 6*y1*M + 2*M_2) + 3*y1_2*pow(y1 - 2*M,2)*log_y1__y1_m_2M))/(16.*pow(y1 - 2*M,2)*M_2);
  metric[3] = (-2*y1*a*M*sin_y2_2)/(y1_2 + a_2*cos_y2_2);
  metric[12] = (-2*y1*a*M*sin_y2_2)/(y1_2 + a_2*cos_y2_2);
  metric[15] = (5*hair*(-1 + 3*cos_y2_2)*sin_y2_m2*(2*M*(-3*y1_2 - 3*y1*M + 2*M_2) + 3*y1*(y1_2 - 2*M_2)*log_y1__y1_m_2M))/(16.*y1_3*M_2) + sin_y2_2*(y1_2 + a_2 + (2*y1*a_2*M*sin_y2_2)/(y1_2 + a_2*cos_y2_2));
}
