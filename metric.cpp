#include "metric.h"


void (*CalculateChristoffelSymbols)(double, double, double, const double*, std::vector<double>&) = 0;
void (*CalculateMetricElements)(double, double, double, const double*, std::vector<double>&) = 0;
void (*CalculateMetricElements3)(double, double, double, const double*, std::vector<double>&) = 0;
