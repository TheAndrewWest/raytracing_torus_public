//Written by A.T. West

#include <stdlib.h>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <cmath>
#include <math.h>

class Basis
{
private:

public:
    
    Basis()
    {
        
    };
    
    double dot4v(double gmunu[4][4], double g00, double g03, double g11, double g22, double g33, double vec1[4], double vec2[4]);
    
//    void gsOrthog(double gmunu[4][4], double g00, double g03, double g11, double g22, double g33, double v0[4], double v1[4], double v2[4], double v3[4], double emu[4][4]);
    void gsOrthog(double gmunu[4][4], double v0[4], double v1[4], double v2[4], double v3[4], double emu[4][4]);
    
};
