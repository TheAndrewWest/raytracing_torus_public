#ifndef XTRACK_METRIC_H_INCLUDED
#define XTRACK_METRIC_H_INCLUDED


#include <vector>


extern void (*CalculateChristoffelSymbols)(double, double, double, const double*, std::vector<double>&);
extern void (*CalculateMetricElements)(double, double, double, const double*, std::vector<double>&);
extern void (*CalculateMetricElements3)(double, double, double, const double*, std::vector<double>&);


inline int make_christoffel_index(int rho, int mu, int nu)
{
  return nu+(4*(mu+4*rho));
}


inline int make_metric_index(int mu, int nu)
{
  return nu+4.*mu;
}


#endif // XTRACK_METRIC_H_INCLUDED
