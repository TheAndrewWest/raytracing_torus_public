//
//  torus.h
//  
//
//  Created by Andrew West on 6/22/19.
//



#include <stdio.h>
#include <math.h>

class Torus
{
private:
    
public:
    Torus()
    {
        
    };
    //double torusHeight;
    double surface(double h, double risco, double r0, double radius, double theta, double phi);
    void emitPosition(double h, double risco, double r0, double rho, double y[10]);
    void tangent(double h, double risco, double r0, double radius, double theta, double phi, double t[4]);
    void tetrad(double g00,double g03,double g11,double g22, double g33, double omega, double rtan[4], double emu[4][4]);
    double TorTheta(double h, double risco, double r0,double rho);
    double TorRad(double h, double risco, double r0,double rho);

//    double rad(double h, double risco, double r0, double radius, double theta, double phi, double t[4]);
//    double theta(double h, double risco, double r0, double radius, double theta, double phi, double t[4]);
};
