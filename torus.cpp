//
//  torus.cpp
//  
//
//  Created by Andrew West on 6/22/19.
//

#include "torus.h"
#include <stdlib.h>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <cmath>
#include <math.h>
#include "macro.h"
#include "xTrackMath.h"

using namespace std;

//This analytically describes the surface in the first quadrant
//positive values are above the torus, negative are within it


double Torus::surface(double h, double risco, double r0, double radius, double theta, double phi)
{
    double x = 0.;
    double y = 0.;
    double rho = 0.;
    double x1 = 0.;
    double x2 = 0.;
    double z =0.;
    
    double theta1 = 0.;
    double radius1 = 0.;
    
    //outer edge needs to be adjusted by ISCO;
    r0 -= risco;
    
    x = radius*sin(theta)*cos(phi);
    y = radius*sin(theta)*sin(phi);
    rho = sqrt(x*x+y*y);
    
    x1 = rho-risco-r0/2.;
    x2 = (rho-risco)/r0;
    z = h*sqrt((r0/2.)*(r0/2.)-x1*x1)*sqrt(x2);
    
    theta1 = atan2(rho,z);
    radius1 = rho/sin(theta1);
    
    return theta1;
    
}

void Torus::emitPosition(double h, double risco, double r0, double rho, double y[4])
{
    for(int g=0; g<10; g+=1)
    {
        y[g]=0.;
    }
    double theta1 =0.;
    double radius1=0.;
    double x1 = 0.;
    double x2 = 0.;
    double z =0.;
    
    x1 = rho-risco-r0/2.;
    x2 = (rho-risco)/r0;
    z = h*sqrt((r0/2.)*(r0/2.)-x1*x1)*sqrt(x2);
    
    theta1 = atan2(rho,z);
    radius1 = rho/sin(theta1);
    
    y[0]=0;
    y[1]=radius1;
    y[2]=theta1;
    y[3]=0;
}

void Torus::tangent(double h, double risco, double r0, double radius, double theta, double phi, double t[4])
{
    
    double delta = 0.000000001;

    if (h==0)
    {
        h+=0.01;
    }
//    cout <<"h = " << h<<endl;
//    cout <<"risco = " <<risco<<endl;
//    cout <<"r0 = " << r0 << endl;
    
    double rho = radius*sin(theta);
//
//    cout <<"r = " << radius << endl;
//    cout <<"theta = " << theta<<endl;
//    cout <<"rho = " << rho << endl;
//
    double theta1=0.;
    double rad1=0.;
    double theta2=0.;
    double rad2=0.;
    theta1 = TorTheta(h, risco, r0, rho);
    rad1 = TorRad(h, risco, r0, rho);
    
    double drho = rho + delta;
    theta2 = TorTheta(h, risco, r0, drho);
    rad2 = TorRad(h, risco, r0, drho);
    cout.precision(17);
//    cout <<"TorRad1 = " << rad1 << endl;
//    cout <<"TorTheta1 = " << theta1<<endl;
//    cout <<"TorRad2 = " << rad2 << endl;
//    cout <<"TorTheta2 = " << theta2<<endl;
    
    t[0] = 0.;
    t[1] = (rad2-rad1)/delta;
    t[2] = (theta2-theta1)/delta;
    t[3] = 0.;

//    cout << "t[0] = " << t[0] << endl;
//    cout << "t[1] = " << t[1] << endl;
//    cout << "t[2] = " << t[2] << endl;
//    cout << "t[3] = " << t[3] << endl;
}

void Torus::tetrad(double g00,double g03,double g11,double g22, double g33, double omega, double rtan[4], double emu[4][4])

{
    
//    make sure there are no hold over values in the tetrad
    for (int i =0; i<4; i++){
        for (int j =0; j<4; j++){
            emu[i][j] =0.;
        }
    }
    
    double ddr = rtan[1];
    double ddt = rtan[2];
    
    //first column indexes vector components
    //et
    double N0 = sqrt(-g00-omega*(2*g03+g33*omega));
    emu[0][0] = 1./N0;
    emu[1][0] = 0.;
    emu[2][0] = 0.;
    emu[3][0] = omega/N0;
    
    //er
    double N1 = sqrt(ddr*ddr*g11+ddt*ddt*g22);
    emu[0][1] = 0.;
    emu[1][1] = ddr/N1;
    emu[2][1] = ddt/N1;
    emu[3][1] = 0.;
    
    //etheta
    double N2 = sqrt((ddr-ddt)*(ddr-ddt)*g11*g22/(ddr*ddr*g11+ddt*ddt*g22));
    emu[0][2] = 0.;
    emu[1][2] = (ddt/(ddt*g11-ddr*g11))*N2;
    emu[2][2] = (ddr/(ddr*g22-ddt*g22))*N2;
    emu[3][2] = 0.;
    
//    ephi
    double topRoot = (g03*g03-g00*g33)*(omega-1)*(omega-1);
    double bottomRoot = g00+omega*(2*g03+g33*omega);
    double root = sqrt(-topRoot/bottomRoot);

    emu[0][3] = (omega-1)*(g03+g33*omega)/((g00+omega*(2*g03+g33*omega))*root);
    emu[1][3] =0.;
    emu[2][3] =0.;
    emu[3][3] = (g00+g03*omega)*root/((g03*g03-g00*g33)*(omega-1));
    
}

double Torus::TorTheta(double h, double risco, double r0,double rho)
{
    double x1 = rho-risco-r0/2.;
    double x2 = (rho-risco)/r0;
    double z = h*sqrt((r0/2.)*(r0/2.)-x1*x1)*sqrt(x2);
    
//    cout <<endl;
//    cout <<"x1 = " << x1 << endl;
//    cout <<"x2 = " << x2 << endl;
//    cout <<"z = " << z << endl;
//    cout <<"rho = " << rho << endl;
//    cout <<"h = " << h <<endl;
//    cout <<"risco = " << risco << endl;
//    cout <<"r0 = " << r0 << endl;
    double theta1=0.;
    if (z<=0.000001)
    {
        return theta1 = PI/2.;
    }
    else if (z>0.000001)
    {
        return theta1 = atan2(rho,z);
    }
}

double Torus::TorRad(double h, double risco, double r0,double rho)
{
    double x1 = rho-risco-r0/2.;
    double x2 = (rho-risco)/r0;
    double z = h*sqrt((r0/2.)*(r0/2.)-x1*x1)*sqrt(x2);
    double theta;
    double rad;
    if (z<=0.000001)
    {
        theta = PI/2.;
        rad = rho;
    }
    else if (z>0.000001)
    {
        theta = atan2(rho,z);
        rad = rho/sin(theta);
    }

//    double rad =0.;
    return rad;
}
