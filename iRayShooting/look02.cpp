#include <TH2.h>
#include <TF1.h>
#include <TProfile.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TRandom3.h>
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include "TMultiGraph.h"
#include "TLegend.h"
#include <TGraphErrors.h>
#include <TGraphAsymmErrors.h>
#include <TRef.h>
#include "TColor.h"


//C++
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <cmath>
//#include <array>

#include <vector>
#include <map>

// C
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>


using namespace std;

#include "model.h"

#define PI 3.141592653589793

void style(TStyle *mystyle);
void wait();

void look02(int a)
{
    gROOT->Reset();

    TStyle *mystyle = new TStyle("mystyle","my own style");
    style(mystyle);

    Int_t MyPalette[100];
    Double_t red[]    = {0., 0.0, 1.0, 1.0, 1.0};
    Double_t green[]    = {0., 0.0, 0.0, 1.0, 1.0};
    Double_t blue[]    = {0., 1.0, 0.0, 0.0, 1.0};
    Double_t stop[] = {0., .25, .50, .75, 1.0};
    Int_t FI = TColor::CreateGradientColorTable(5, stop, red, green, blue, 100);
    for (int i=0;i<100;i++) MyPalette[i] = FI+i;
    
    gStyle->SetPalette(100, MyPalette);

    TCanvas *c01[2];
    c01[0] = new TCanvas("c01a","Analysis",10,10,4508,4640);
    c01[1] = new TCanvas("c01b","Analysis",610,10,4608,4640);

    for (int i=0;i<2;i++)
    {
        c01[i]->SetLeftMargin(0.2); c01[i]->SetRightMargin(0.08);
        c01[i]->SetTopMargin(0.1); c01[i]->SetBottomMargin(0.25);
        c01[i]->Divide(3,3,0,0);
    }

    gPad->SetTicks(1,1);

    mystyle->SetLabelSize(.08, "x");
    mystyle->SetLabelSize(.08, "y");
    
    mystyle->SetLabelOffset(0.02, "x");
    mystyle->SetLabelOffset(0.01, "y");

    mystyle->SetTitleXOffset(1.1); // 1.5
    mystyle->SetTitleYOffset(0.7); // 1.9

    mystyle->SetTitleSize(0.08, "X");
    mystyle->SetTitleSize(0.08, "Y");

    mystyle->cd(); gROOT->ForceStyle();

//    TCanvas *c02 = new TCanvas("c02","Analysis",10,10,600,600);
//    gPad->SetTicks(1,1); c02->SetLeftMargin(0.2); c02->SetRightMargin(0.08);
    
    gDirectory->pwd();
    gPad->SetLogx(0);

    int IND1[9]={40,44,48,52,56,60,64,68,72}; // 68 is missing
    TH2F *ps1[2][9];

    for (int i=0;i<2;i++)
        for (int j=0;j<9;j++)
        {
            int index=i+IND1[j];
            double gamma,kappa_c,kappa_s,sfx,sfy;
            model(index,kappa_c,gamma,kappa_s,sfx,sfy);
            int image=index%4;
            char fname[50]; sprintf(fname,"/data/krawcz/rt03/out/rt%02d_%04d.root",index,100);
            cout <<"opening "<<fname<<endl;
            TFile file(fname,"READ");

            cout <<"Looking at "<<
                    " image   = "<<image<<
                    " kappa_c = "<<kappa_c<<
                    " gamma   = "<<gamma<<
                    " kappa_s = "<<kappa_s<<endl;
        
        
            if (!file.IsZombie())
            {
                char hname[50];
                sprintf(hname,"ps1%1d",1);
                if (file.GetListOfKeys()->Contains(hname))
                {
/*                    int k =2*j+1;
                    if (k>8) k-=7;
                    cout <<k<<endl;*/
                    c01[i]->cd(1+j);

                    if (j==0)
                    {
                        mystyle->SetLabelSize(.08, "x");
                        mystyle->SetLabelSize(.08, "y");
                        mystyle->SetTitleSize(0.08, "X");
                        mystyle->SetTitleSize(0.08, "Y");
                        
                        mystyle->SetLabelOffset(0.02, "x");
                        mystyle->SetLabelOffset(0.01, "y");
                        
                        mystyle->SetTitleXOffset(1.1); // 1.5
                        mystyle->SetTitleYOffset(0.7); // 1.9
                    }
                    if (j==6)
                    {
                        mystyle->SetLabelSize(.06, "x");
                        mystyle->SetLabelSize(.06, "y");
                        mystyle->SetTitleSize(0.06, "X");
                        mystyle->SetTitleSize(0.06, "Y");
                    }
                    if (j==7)
                    {
                        mystyle->SetLabelSize(.076, "x");
                        mystyle->SetLabelSize(.076, "y");
                        mystyle->SetTitleSize(0.076, "X");
                        mystyle->SetTitleSize(0.076, "Y");
                        
                        mystyle->SetLabelOffset(0.005, "x");
                        mystyle->SetTitleXOffset(0.8); // 1.5

                    }

                    ps1[i][j] = (TH2F*)file.Get(hname);
                    ps1[i][j]->SetStats(kFALSE);
                    ps1[i][j]->SetDirectory(0);
                
                    char newName[100];
                    sprintf(newName,"h%02d",index);
                    ps1[i][j]->SetName(newName);
                    ps1[i][j]->SetTitle(";y_{1}  ;y_{2 } ");
                    ps1[i][j]->GetXaxis()->SetRangeUser(-3.97,3.97);
                    ps1[i][j]->GetYaxis()->SetRangeUser(-3.97,3.97);

//                    mystyle->cd(); gROOT->ForceStyle();

                    ps1[i][j]->DrawCopy("colz");
                    c01[i]->cd();
                    c01[i]->Modified();c01[i]->Update();
                }
                file.Close();
            }
        }
    c01[0]->SaveAs("mm1.png");
    c01[1]->SaveAs("mm2.png");
}

void style(TStyle *mystyle)
{
    
    //Datum unten links einblenden
    mystyle->SetOptDate(0);
    
    //Farbe des Canvas
    mystyle->SetCanvasColor(0);
    
    //Rahmen um den Canvas an/aus
    mystyle->SetCanvasBorderMode(0);
    //Dicke des Canvas Rahmens, falls eingeschaltet
    mystyle->SetCanvasBorderSize(1);
    //default Groesse des Canvas
    mystyle->SetCanvasDefH(500);
    mystyle->SetCanvasDefW(500);
    
    //Rahmen um Histos...
    mystyle->SetFrameBorderMode(0);
    mystyle->SetFrameBorderSize(1);
    
    //dasselbe fuer Pads im Canvas
    mystyle->SetPadColor(0);
    mystyle->SetPadBorderMode(1);
    mystyle->SetPadBorderSize(1);
    
    //Histo Styles
    //Histo is filled with light grey
    // mystyle->SetHistFillColor(4); //14 dunkel grau
    // make line a bit thicker...
    mystyle->SetHistLineWidth(2);
    // default line color of the histo...
    mystyle->SetHistLineColor(1);
    // make axis labels a bit bigger (NUMBERS)...
    mystyle->SetLabelSize(.04, "x");
    mystyle->SetLabelOffset(0.02, "x");
    mystyle->SetLabelSize(.04, "y");
    mystyle->SetLabelOffset(0.03, "y");
    
    // Fill Color of title and stat boxes
    mystyle->SetTitleColor(1);
    mystyle->SetStatColor(0);
    
    // if Polymarkers are to be used, e.g. for display with errors,
    // use these
    // filled circles...
    //    mystyle->SetMarkerStyle(8);
    //    mystyle->SetMarkerSize(1.);
    //    mystyle->SetMarkerColor(4);
    //     mystyle->SetErrorMarker(21); //??
    //     mystyle->SetErrorMsize(5.);// ??
    //set the errors in x to be as broad as the binsize
    //   mystyle->SetErrorX(1.);
    
    
    // multipliy the default size...
    //   mystyle->SetScreenFactor(2);
    
    //decrease plot size for better labelling
    mystyle->SetPadBottomMargin(0.2);
    mystyle->SetPadLeftMargin(0.2);
    
    // set x/y-label lower to be seen under the numbers...
    mystyle->SetTitleXOffset(1.3); // 1.5
    mystyle->SetTitleYOffset(1.9); // 1.9
    // text size of axis labels...
    mystyle->SetTitleSize(0.045, "X");
    mystyle->SetTitleSize(0.045, "Y");
    mystyle->cd();
    // Force the Style for the histos...
//    gROOT->ForceStyle();
}

void wait()
{
    char in[50];
    cout << "Please enter something "<<endl;
    cin >> in;
}
