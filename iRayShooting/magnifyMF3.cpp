#include <TAxis.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TH1F.h>
#include <TH2.h>
#include "TColor.h"
#include "TFile.h"
#include "TStyle.h"
#include <TRandom3.h>

//C++
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <cmath>
//#include <array>

#include <vector>

// C
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>

using namespace std;

// #include "magnify.h"
#define PI 3.141592653589793

void magnify(long random,int index, int mi,double nM, int Flag);

void model(int index, double &kc, double &g, double &ks, double &sfx,double &sfy);
void dai(int fi,int img, double &kc, double &g, double &ks);
void blackburne(int gi,int img, double &kc, double &g, double &ks);
void smooth(TH2F *ps2,TH2F *ps3,double Gamma);
void wait();
double ran1(long *idum);

void go(int index,int mi=100, double nM=1000.)
{
    system("date");
    magnify(-2*index-100,index,mi,nM,3);
    system("date");
}

void run(int mi=100, double nM=1000.)
{
//    for (int index=0;index<76;index++)
    for (int index=0;index<76;index++)
    {
        system("date");
        magnify(-10, // random seed
                index,
                mi, // mscale*1000
                nM,
                3); // Flag =1 only print, Flag=2 first iteration.
        cout<<endl;
    }
}

void getXiEta(vector<double>& mass,vector<double>& starX,vector<double>& starY,
              double x,double y,double &xi,double &eta)
{
    // sum over stars
    xi = 0;
    eta = 0;
    for (size_t i = 0; i < mass.size(); ++i)
    {
        double norm = mass[i]/
        ((starX[i] - x) * (starX[i] - x) +
         (starY[i] - y) * (starY[i] - y));
        xi  += norm*(starX[i] - x);
        eta += norm*(starY[i] - y);
    }
}

void getXiEta(double kappa_c,double gamma,
              vector<double>& mass,vector<double>& starX,vector<double>& starY,
              double x,double y,double &xi,double &eta)
{
    getXiEta(mass,starX,starY,x,y,xi,eta);
    
    // add the shear terms
    xi  += (1 - kappa_c - gamma) * x;
    eta += (1 - kappa_c + gamma) * y;
}

// class for calculating exponential running mean
class GetY {
    vector<double> nearX;
    vector<double> nearY;
    vector<double> nearMass;

    vector<double> farX;
    vector<double> farY;
    vector<double> farMass;

    double kappa_c;
    double gamma;
    
    double x1,x2,y1,y2,dX,dY;
    double etaE[4];
    double xiE[4];
    int nNear;
    
public:
    GetY(double xc,double brX, double yc,double brY,
         double kappa_c, double gamma,double n_s,
         vector<double>& mass,vector<double>& starX,vector<double>& starY);
    
    void getY(double x,double y,double &xi,double &eta);

    int getNNear(){return nNear;};

};

GetY::GetY(double xc,double brX, double yc,double brY,
     double kc, double g,double n_s,
     vector<double>& mass,vector<double>& starX,vector<double>& starY)
{
    kappa_c=kc;
    gamma=g;
    
    double area = 10./n_s;
    double radiusSQ = area/4.;
    
    double r = (brX>brY) ? brX : brY;
    r*=30.;

    double r2=r*r;
    radiusSQ = (radiusSQ>r2) ? radiusSQ : r2;
    
    for (size_t i=0;i<mass.size();i++)
    {
        double dx=starX[i]-xc;
        double dy=starY[i]-yc;
        double distanceSQ=dx*dx+dy*dy;
        if (distanceSQ<radiusSQ)
        {
            nearMass.push_back(mass[i]);
            nearX.push_back(starX[i]);
            nearY.push_back(starY[i]);
        }
        else
        {
            farMass.push_back(mass[i]);
            farX.push_back(starX[i]);
            farY.push_back(starY[i]);
        }
    }
    
    for (int i=0;i<4;i++)
    {
        double x,y;
        if (i==0)
        {
            x=x1=xc-brX;
            y=y1=yc-brY;
        }
        if (i==1)
        {
            x=x2=xc+brX;
            y=y1;
        }
        if (i==2)
        {
            x=x1;
            y=y2=yc+brY;
        }
        if (i==3)
        {
            x=x2;
            y=y2;
        }
        
        getXiEta(kappa_c,gamma,
                farMass,farX,farY,
                x,y,xiE[i],etaE[i]);
    }
    dX=x2-x1;
    dY=y2-y1;
    nNear=nearMass.size();
    
    /*
    cout <<"Number of nearby stars "<<nearMass.size()<<endl;
    cout <<"Number of faraway stars "<<farMass.size()<<endl;
    cout <<"Upper left edge "<<x1<<" "<<y1<<endl;
    cout <<"lower right edge "<<x2<<" "<<y2<<endl;
    cout <<"Results at four edges: "
    <<xiE[0]<<" "<<etaE[0]<<" "
    <<xiE[1]<<" "<<etaE[1]<<" "
    <<xiE[2]<<" "<<etaE[2]<<" "
    <<xiE[3]<<" "<<etaE[3]<<endl;
     */
}

void GetY::getY(double x,double y,double &xi,double &eta)
{
    double xi3,eta3;
    
    /*
    cout <<"getY for x= "<<x<<" y= "<<y<<endl;
    
     double xi2,eta2;
    getXiEta(kappa_c,gamma,
             farMass,farX,farY,
             x,y,xi2,eta2);
    cout <<"macro plus faraway stars: "<<xi2<<" "<<eta2<<endl;
     */
    
    // calculate nearby stars
    getXiEta(nearMass,nearX,nearY,x,y,xi,eta);
    // cout <<"Nearby stars: "<<xi<<" "<<eta<<endl;
    
    // add interpolation of faraway stars
    double valX  = (x2-x)/dX;
    double restX = (1.-valX);
    
    double valY = (y2-y)/dY;
    double restY = (1.-valY);
    
    // cout <<"valX "<<valX<<endl;
    // cout <<"restX "<<restX<<endl;
    // cout <<"valY "<<valY<<endl;
    // cout <<"restY "<<restY<<endl;
    
    double val1 = valX*xiE[0]+restX*xiE[1];
    double val2 = valX*xiE[2]+restX*xiE[3];

    xi3 = valY * val1 + restY * val2;

    val1 = valX*etaE[0]+restX*etaE[1];
    val2 = valX*etaE[2]+restX*etaE[3];
    
    eta3 = valY * val1 + restY * val2;
    // cout <<"Interpolation "<<xi3<<" "<<eta3<<endl;
    
    xi=xi+xi3;
    eta=eta+eta3;
    // cout <<"After adding faraway stars: "<<xi<<" "<<eta<<endl;
}
void magnify(long random,int index, int mi,double nM, int Flag)
{
    static long seed = (long)random;
    double Ls=20.;
    double Gamma=2.5;
    unsigned long long nrays = 1000000;
    double mScale=(double)mi/1000;
//    double rg=0.000604488;

    int Nreg=40.;
    double rg=0.000955784*sqrt(mScale/0.1);
    double innerRegion=4.;
    
    double gamma,kappa_c,kappa_s,sfx,sfy;
    model(index,kappa_c,gamma,kappa_s,sfx,sfy);
    
    // define source and lens plane geometries
    
    // double scale_x =SX*fabs(1./(1 - (kappa_c+kappa_s) - gamma));
    // double scale_y =SY*fabs(1./(1 - (kappa_c+kappa_s) + gamma));

    double scale_x =fabs(1./(1 - kappa_c - gamma));
    double scale_y =fabs(1./(1 - kappa_c + gamma));
    
    if (scale_x>10.) scale_x=10.;
    if (scale_y>10.) scale_y=10.;

    scale_x *=sfx;
    scale_y *=sfy;

    cout<<"Scale factor source->lens (x;y) "<<scale_x<<" ; "<<scale_y<<" overwrite "<<sfx<<" "<<sfy<<endl;
    
    double xrange = 2.*Ls*scale_x;
    double yrange = 2.*Ls*scale_y;
    cout <<"Lens plane area (xrange x yrange): "<<xrange<<" x "<<yrange<<endl;
    
    // constants for Stellar Mass Function
    double m1 = mScale * 0.111444;
    double m2 = 50.*m1;
    double n0 = 1./(2.3025016842966943/pow(m1,0.3));
    
    // number of stars one should simulate
    double sRangeX=xrange*2.;
    double sRangeY=yrange*2.;
    double n_s = kappa_s/PI;
    double n1 = (sRangeX*sRangeY*n_s);
    
    // however, we do not want to simulate less than 100 stars
    
    double scf=1.;
    if (n1<100)
    {
        scf = sqrt(100/(double)n1);
        sRangeX*=scf;
        sRangeY*=scf;
        n1=100;
    }
    int n2=(int)(n1+0.5);
    
    cout <<"Star area (sRangeX x sRangey): "<<sRangeX<<" x "<<sRangeY<<" = "<<n2<<" stars "<<" (scf "<<scf<<")"<<endl;
    
    double steps  = sqrt((double)nrays);
    double deltaX = xrange/steps;
    double deltaY = yrange/steps;
    
    cout <<"Stars per bin: "<<deltaX*deltaY*n_s<<endl;

    if (Flag==1) return;

    // define a new palette
    
    Int_t MyPalette[100];
    Double_t red[]    = {0., 0.0, 1.0, 1.0, 1.0};
    Double_t green[]    = {0., 0.0, 0.0, 1.0, 1.0};
    Double_t blue[]    = {0., 1.0, 0.0, 0.0, 1.0};
    Double_t stop[] = {0., .25, .50, .75, 1.0};
    Int_t FI = TColor::CreateGradientColorTable(5, stop, red, green, blue, 100);
    for (int i=0;i<100;i++) MyPalette[i] = FI+i;
    
    gStyle->SetPalette(100, MyPalette);

    // open output files
    char  fname[100];
    
    sprintf(fname,"out/rt%02d_%04d.root",index,mi);
    TFile out(fname,"recreate");
    out.cd();

    TCanvas *canvas0 = new TCanvas("canvas0","Star Map",5,5, 500, 500);
    TCanvas *canvas3 = new TCanvas("canvas3","SMF", 500,5,500, 500);

    TCanvas *canvas4 = new TCanvas("canvas4","canvas4", 2000,200,500, 500);

    TCanvas *canvas5a = new TCanvas("canvas5a","LensMap"          ,10,400,500, 500);
    TCanvas *canvas5b = new TCanvas("canvas5b","Smoothed LensMap",210,400,500, 500);
    TCanvas *canvas5c = new TCanvas("canvas5c","Truncated Smoothed LensMap",410,400,500, 500);

    TCanvas *canvas6 = new TCanvas("canvas6","LensMap Distribution",600,400,500, 500);
    
    TH2F *ps0 = new TH2F("PS0",";x_{1};x_{2}",100,-xrange/2.,xrange/2.,100,-yrange/2.,yrange/2.);
    
    vector <double> starX(n2);
    vector <double> starY(n2);
    vector <double> mass(n2);
    
    double mSum=0.;
    
    // set up stars, histogram with stellar mass distribution
    TH1F *hSMF = new TH1F("hSMF", ";log10(Mass/Msun); Number [a.u.]",16, -3.,2.);

    for (int i=0;i<n2;i++)
    {
        double x,y;
        
        x=(ran1(&seed)-0.5)*sRangeX;
        y=(ran1(&seed)-0.5)*sRangeY;
        
        starX[i]=x;
        starY[i]=y;
        
        double yRan = ran1(&seed);
        mass[i]=pow(pow(m1,-0.3)-3./10.*yRan/n0,-1./0.3)/mScale;
        
        mSum+=mass[i];
        hSMF->Fill(log10(mass[i]));
        
        if (i<10) cout <<"Star "<<i<<" "<<" M "<<mass[i]
            <<" x "<<starX[i]
            <<" y "<<starY[i]
            <<endl;
        ps0->Fill(starX[i],starY[i],mass[i]);
    }
    
    cout <<"Created "<<n2<<" stars "<<endl;
    cout <<"Mean mass of stars "<<mSum/(double)n2<<" x "<<mScale<<" solar masses"<<endl;
    cout <<"m1 "<<m1<<endl;
    cout <<"m2 "<<m2<<endl<<endl;
    
    canvas0->cd(); ps0->DrawCopy("colz");
    canvas0->Modified();canvas0->Update();

    canvas3->cd();hSMF->DrawCopy();
    canvas3->Modified();canvas3->Update();
    
    double etaR[Nreg];
    double xiR[Nreg];
    vector<TH2F*> magMapList;
    
    double Rreg      = 4.*100.*rg;
    int    nBins     = 2.*Rreg/(0.2*rg);
    
    for (int i=0;i<Nreg;i++)
    {
        etaR[i] = (ran1(&seed)-0.5)*2.*(innerRegion-Rreg);
        xiR[i]  = (ran1(&seed)-0.5)*2.*(innerRegion-Rreg);
        
        char name [100]; sprintf(name ,"magMapHisto%03d",i);
        char name2[100]; sprintf(name2,"magMapHisto%03d; %f %f ",i,etaR[i],xiR[i]);
        
        TH2F *h = new TH2F(name,name2,nBins,-Rreg,Rreg,nBins,-Rreg,Rreg);
        magMapList.push_back(h);
        
        if (i<10) cout<<"Region "<<i<<" eta "<<etaR[i]<<" "<<xiR[i]<<endl;
    }
    cout <<"Regions created"<<endl;

    TGraph *gr;
    gr = new TGraph(Nreg,etaR,xiR);
    gr->SetMarkerStyle(24);
    gr->SetMarkerSize(0.8);
    gr->SetMarkerColor(2);
    
    double Rreg2 = Rreg*Rreg;
    
#define numC 5
    double side[numC]={50.,10.,1.,0.1,0.03};
    int bins[numC]={800,800,800,400,100};
    TH2F *ps1[numC];
    TCanvas *canvasA[numC];
    
    for (int i=0;i<numC;i++)
    {
        char name[100];
        char name2[100];
        sprintf(name,"ps1%1d",i);
        ps1[i] = new TH2F(name,";y_{1};y_{2}",
                          bins[i],-side[i],side[i],
                          bins[i],-side[i],side[i]);
        ps1[i]->SetStats(kFALSE);
        sprintf(name,"canvasA-%1d",i);
        sprintf(name2,"canvasA-%f",side[i]);
        canvasA[i]= new TCanvas(name,name2, 5+i*50,5,500,500);
    }
    
    TH2F *ps5 = new TH2F("PS5",";x_{1};x_{2}",
                   1000,-xrange/2.,xrange/2.,
                   1000,-yrange/2.,yrange/2.); // 1000
    TH2F *ps5b = new TH2F("PS5b",";x_{1};x_{2}",
                   1000,-xrange/2.,xrange/2.,
                   1000,-yrange/2.,yrange/2.); // 1000
    TH2F *ps5c = new TH2F("PS5c",";x_{1};x_{2}",
                          1000,-xrange/2.,xrange/2.,
                          1000,-yrange/2.,yrange/2.); // 1000
    // histogram with stellar mass distribution
    TH1F *hPS5 = new TH1F("hPS5", ";log10(entries/bin); Occurrence",100,-5.,12.);
    TH1F *hDelta = new TH1F("hDelta", ";log10(distance in source plane); Occurrence",100,-5.,12.);
    
#define SLICE
#ifdef SLICE
#define numSlice 11
    double sliceDX=innerRegion-Rreg;
    double sliceDY=innerRegion-Rreg;
    
    vector<TH1F*> slice;
    vector<double> sliceY1;
    vector<double> sliceY2;
    
    for (int i=0;i<numSlice;i++)
    {
        char name[100];
        sprintf(name,"slice%02d",i);
        slice.push_back(new TH1F(name,";x;entries",2.*sliceDX/rg,-sliceDX,sliceDX));
        
        double yCoord=-sliceDY+(2.*sliceDY/(double)(numSlice+1))*(double)i;
        
        double y1=yCoord-15.*rg;
        double y2=yCoord+15.*rg;
        
        sliceY1.push_back(y1);
        sliceY2.push_back(y2);
    }
#endif
    
    // unsigned long long nr=0;
    for (double x=-xrange/2.; x<=xrange/2.; x+= deltaX) {
        for (double y=-yrange/2.; y<=yrange/2.; y+= deltaY) {
            
            // do the sums over the stars first
            double xi,eta;
            getXiEta(kappa_c,gamma,mass,starX,starY,x,y,xi,eta);
            
            for (int i=0;i<3;i++)
                ps1[i]->Fill(xi,eta);
            
            if ((fabs(xi)<innerRegion)&&(fabs(eta)<innerRegion)) ps5->Fill(x,y);
        }
    }

    for (int i=0;i<3;i++)
    {
        canvasA[i]->cd();
        ps1[i]->DrawCopy("colz");
        gr->Draw("P");
        canvasA[i]->Modified(); canvasA[i]->Update();
        sprintf(fname,"out/m_%2d_%1d_%04d.png",index,i,mi);
        canvasA[i]->SaveAs(fname);
        ps1[i]->Reset();
    }

    canvas5a->cd();
    ps5->DrawCopy("colz");
    canvas5a->Modified(); canvas5a->Update();
    
    smooth(ps5,ps5b,Gamma);

    canvas5b->cd(); ps5b->DrawCopy("colz");
    canvas5b->Modified(); canvas5b->Update();
    sprintf(fname,"out/m_%2d_10_%04d.png",index,mi);
    canvas5b->SaveAs(fname);

    // determine the interesting areas in the lense plane
    for (int i=1;i<=ps5b->GetNbinsX();i++)
        for (int j=1;j<=ps5b->GetNbinsY();j++)
        {
            double content=ps5b->GetBinContent(i,j);
            if (content>0.)
                hPS5->Fill(log10(content));
        }

    double sum1=0.;
    for (int i=1;i<=hPS5->GetNbinsX();i++)
        sum1+=hPS5->GetBinContent(i);

    int thr; double sum2=0.;
    for (thr=1;thr<=hPS5->GetNbinsX();thr++)
    {
        sum2+=hPS5->GetBinContent(thr);
        if (sum2>sum1/20.) break;
    }
    if (thr>hPS5->GetNbinsX())thr=hPS5->GetNbinsX();
    double bThr=pow(10.,hPS5->GetBinCenter(thr));
    cout <<"Truncation at "<<bThr<<" "<<0.5<<endl;
    bThr=0.25;
    // done
    
    // mark all bins above bThr
    for (int i=1;i<=ps5b->GetNbinsX();i++)
        for (int j=1;j<=ps5b->GetNbinsY();j++)
        {
            double content=ps5b->GetBinContent(i,j);
            if (content>bThr)
                ps5c->SetBinContent(i,j,content);
        }

    canvas5c->cd(); ps5c->DrawCopy("colz");
    canvas5c->Modified(); canvas5c->Update();
    sprintf(fname,"out/m_%2d_11_%04d.png",index,mi);
    canvas5c->SaveAs(fname);

    canvas6->cd();
    hPS5->DrawCopy();
    canvas6->Modified();canvas6->Update();
    
    if (Flag==2) return;
    
    // second iteration
    double brX=(ps5c->GetXaxis()->GetBinCenter(2)-ps5c->GetXaxis()->GetBinCenter(1))/2.;
    double brY=(ps5c->GetYaxis()->GetBinCenter(2)-ps5c->GetYaxis()->GetBinCenter(1))/2.;
    double DeltaX=brX/nM; // 20
    double DeltaY=brY/nM; // 20
    
    int first=1;
    long int counter=0;
    
    for (int iB=1;iB<=ps5c->GetNbinsX();iB++)
        for (int jB=1;jB<=ps5c->GetNbinsY();jB++)
        {
            float content=ps5c->GetBinContent(iB,jB);
            if (content>0.)
            {
                double xc = ps5c->GetXaxis()->GetBinCenter(iB);
                double yc = ps5c->GetYaxis()->GetBinCenter(jB);
                
                // make new object to get xi and eta for points within bin
                GetY *get = new GetY(xc,brX,yc,brY,kappa_c,gamma,n_s,mass,starX,starY);
                if (first){first=0; cout<<"Number of near stars "<<get->getNNear()<<endl;}
                
                for (double x=xc-brX+DeltaX/2.; x<xc+brX; x+= DeltaX)
                {
                    for (double y=yc-brY+DeltaY/2.; y<yc+brY; y+= DeltaY)
                    {
                        double xi,eta;
                        get->getY(x,y,xi,eta);

                        if (counter++>50000)
                        {
                            counter=0;
                         
                            double xi2,eta2;
                            getXiEta(kappa_c,gamma,mass,starX,starY,x,y,xi2,eta2);
                            
                            double delta=sqrt((xi-xi2)*(xi-xi2)+(eta-eta2)*(eta-eta2));
                            
                            if (delta>1E-5) hDelta->Fill(log10(delta));
                            else hDelta->Fill(-4.99999);
                        }
                        
                        for (int i=0;i<numC;i++)
                            ps1[i]->Fill(xi,eta);

#ifdef SLICE
                        for (size_t i=1;i<slice.size();i++)
                            if ((eta>sliceY1[i])&&(eta<sliceY2[i]))
                                slice[i]->Fill(xi);
#endif
                        for (int i=0;i<Nreg;i++)
                        {
                            double dxi=xiR[i]-xi;
                            double deta=etaR[i]-eta;
                            
                            if (dxi*dxi+deta*deta<Rreg2)
                                magMapList[i]->Fill(dxi,deta);
                        }
                    }
                }
                delete get;
            }
        }
   
    canvas6->cd();
    hDelta->DrawCopy();
    canvas6->Modified();canvas6->Update();

    // plot modified panorama maps
    for (int i=0;i<numC;i++)
    {
        canvasA[i]->cd();
        ps1[i]->DrawCopy("colz");
        gr->Draw("P");
        canvasA[i]->Modified(); canvasA[i]->Update();
        sprintf(fname,"out/m_%2d_%1d_%04dr.png",index,i,mi);
        canvasA[i]->SaveAs(fname);
    }
    
    // plot modified region maps
    canvas4->cd();
    for (auto i : magMapList )
    {
        i->DrawCopy("colz");
        canvas4->Modified();canvas4->Update();
    }
    
    // write results
    ps0->Write();
    
    for (int i=0;i<numC;i++)
        ps1[i]->Write();

#ifdef SLICE
    for (size_t i=0;i<slice.size();i++)
        slice[i]->Write();
#endif
    
    for (auto i : magMapList )
        i->Write();

    ps5->Write();
    ps5b->Write();
    ps5c->Write();
    hPS5->Write();
    
    hDelta->Write();

    gr->Write();
    
    out.Close();
}

void model(int index, double &kc, double &g, double &ks, double &sfx,double &sfy)
{
    sfx=sfy=1.;
    
    if (index==0) {sfx=9.;sfy=0.7;}
    if (index==3) {sfx=0.7;}
    if (index==4) sfx=10.;
    if (index==7) {sfx=0.7;}
    if (index==8) sfx=2.5;
    if (index==11) {sfx=0.7;}
    if (index==12) sfx=2.;
    if (index==15) {sfx=0.7;}
    if (index==19) {sfx=0.5;}
    if (index==23) {sfx=0.5;}
    if (index==27) {sfx=0.5;}
    if (index==31) {sfx=0.5;}
    if (index==35) {sfx=0.5;}
    if (index==39) {sfx=0.5;}
    if (index==40) sfx=2.;
    if (index==44) sfx=4.;
    if (index==48) sfx=15.;
    if (index==55) {sfx=0.7;}
    if (index==59) {sfx=0.5;}
    if (index==63) {sfx=0.5;sfy=2.;}
    if (index==67) {sfx=0.4;sfy=2.;}
    if (index==68) {sfy=1.2;}
    if (index==71) {sfx=0.35;sfy=2.5;}
    if (index==72) {sfy=1.2;}
    if (index==75) {sfx=0.7;sfy=2.;}
    
    
    if (index<40)
    {
        int fi=(int)(index/4);
        int img=index-fi*4;
        dai(fi,img,kc,g,ks);
        cout <<"Dai "<<index<<" fi "<<fi<<" img "<<img<<" kc "<<kc<<" g "<<g<<" ks "<<ks<<endl;
    }
    else
    {
        index-=40;
        int gi=(int)(index/4);
        int img=index-gi*4;
        blackburne(gi,img,kc,g,ks);
        cout <<"Bla "<<index<<" gi "<<gi<<" img "<<img<<" kc "<<kc<<" g "<<g<<" ks "<<ks<<endl;
    }
}

void dai(int fi,int img, double &kc, double &g, double &ks)
// fi: f_* index, 0...9, img: image index, 0.,... 3
{
    double Table1[40][4]={
        {1, 0.667, 0.359, 0.030},
        {2, 0.631, 0.325, 0.027},
        {3, 0.644, 0.306, 0.028},
        {4, 1.079, 0.493, 0.079},
        {1, 0.618, 0.412, 0.067},
        {2, 0.581, 0.367, 0.060},
        {3, 0.595, 0.346, 0.062},
        {4, 1.041, 0.631, 0.159},
        {1, 0.569, 0.465, 0.110},
        {2, 0.530, 0.410, 0.099},
        {3, 0.546, 0.387, 0.103},
        {4, 1.001, 0.635, 0.242},
        {1, 0.519, 0.518, 0.162},
        {2, 0.480, 0.453, 0.146},
        {3, 0.496, 0.427, 0.153},
        {4, 0.964, 0.895, 0.329},
        {1, 0.469, 0.572, 0.226},
        {2, 0.430, 0.497, 0.204},
        {3, 0.447, 0.469, 0.214},
        {4, 0.925, 1.018, 0.421},
        {1, 0.419, 0.626, 0.305},
        {2, 0.379, 0.540, 0.278},
        {3, 0.397, 0.511, 0.290},
        {4, 0.890, 1.139, 0.520},
        {1, 0.369, 0.679, 0.406},
        {2, 0.329, 0.584, 0.375},
        {3, 0.348, 0.552, 0.390},
        {4, 0.851, 1.288, 0.625},
        {1, 0.318, 0.734, 0.541},
        {2, 0.278, 0.628, 0.507},
        {3, 0.297, 0.595, 0.524},
        {4, 0.816, 1.412, 0.740},
        {1, 0.268, 0.787, 0.725},
        {2, 0.228, 0.671, 0.697},
        {3, 0.247, 0.637, 0.711},
        {4, 0.781, 1.530, 0.863},
        {1, 0.217, 0.842, 1.000},
        {2, 0.178, 0.714, 1.000},
        {3, 0.196, 0.679, 1.000},
        {4, 0.740, 1.639, 1.000}};
    
    int i=fi*4+img;
    ks = Table1[i][1]*Table1[i][3];
    g  = Table1[i][2];
    kc = Table1[i][1]-ks;
}

void blackburne(int gi,int img, double &kc, double &g, double &ks)
// gi, g_* index, 0...,8
{
    double Table1[4][3]={{1,0.442,0.597},{2,0.423,0.507},{3,0.422,0.504},{4,0.834,0.989}};
    double Table2[9]={0.025,0.05,0.1,0.25,0.5,0.75,0.9,0.95,0.975};
    
    kc=Table1[img][1];
    g=Table1[img][2];
    ks=Table2[gi]*kc;
    kc-=ks;
}

void smooth(TH2F *ps2,TH2F *ps3,double Gamma)
{
    int offset=15;
    int imax=ps2->GetNbinsX();
    int jmax=ps2->GetNbinsY();
    
    double w[offset+1][offset+1];
    
    for (int i=0; i<=offset; i++)
        for (int j=0; j<=offset; j++)
        {
            w[i][j]=1./(PI*Gamma*(1+(double)(i*i+j*j)/(Gamma*Gamma)));
        }
    
    for (int i=1; i<=imax; i++)
    {
        for (int j=1; j<=jmax; j++)
        {
            double sum=0.;
            double weight=0.;
            
            for (int di=-offset; di<=offset;di++)
                for (int dj=-offset; dj<=offset;dj++)
                {
                    int ii=i+di;
                    int jj=j+dj;
                    if ((ii>0)&&(ii<=imax)&&(jj>0)&&(jj<=jmax))
                    {
                        
                        double ww=w[abs(di)][abs(dj)];
                        sum += ww * ps2->GetBinContent(ii,jj);
                        weight += ww;
                    }
                }
            
            double res=0.;
            
            if (weight>0.) res=sum/weight;
            ps3->SetBinContent(i,j,res);
        }
    }
}

/*
 double ran1()
 {
 return (double)rand()/(double)RAND_MAX;
 }
 */

/* Random number generator ran1 from Computers in Physics */
/* Volume 6 No. 5, 1992, 522-524, Press and Teukolsky */
/* To generate real random numbers 0.0-1.0 */
/* Should be seeded with a negative integer */
#define IA 16807
#define IM 2147483647
#define IQ 127773
#define IR 2836
#define NTAB 32
#define EPS (1.2E-07)
#define MAX(a,b) (a>b)?a:b
#define MIN(a,b) (a<b)?a:b

double ran1(long *idum)
{
    int j,k;
    static int iv[NTAB],iy=0;
    void nrerror();
    static double NDIV = 1.0/(1.0+(IM-1.0)/NTAB);
    static double RNMX = (1.0-EPS);
    static double AM = (1.0/IM);
    
    if ((*idum <= 0) || (iy == 0)) {
        *idum = MAX(-*idum,*idum);
        for(j=NTAB+7;j>=0;j--) {
            k = *idum/IQ;
            *idum = IA*(*idum-k*IQ)-IR*k;
            if(*idum < 0) *idum += IM;
            if(j < NTAB) iv[j] = *idum;
        }
        iy = iv[0];
    }
    k = *idum/IQ;
    *idum = IA*(*idum-k*IQ)-IR*k;
    if(*idum<0) *idum += IM;
    j = iy*NDIV;
    iy = iv[j];
    iv[j] = *idum;
    return MIN(AM*iy,RNMX);
}
#undef IA
#undef IM
#undef IQ
#undef IR
#undef NTAB
#undef EPS
#undef MAX
#undef MIN

void wait()
{
    char in[50];
    cout << "Please enter something "<<endl;
    cin >> in;
}
