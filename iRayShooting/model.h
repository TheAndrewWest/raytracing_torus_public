void model(int index, double &kc, double &g, double &ks, double &sfx,double &sfy);
void dai(int fi,int img, double &kc, double &g, double &ks);
void blackburne(int gi,int img, double &kc, double &g, double &ks);

void model(int index, double &kc, double &g, double &ks, double &sfx,double &sfy)
{
    sfx=sfy=1.;
    
    if (index==0) {sfx=9.;sfy=0.7;}
    if (index==3) {sfx=0.7;}
    if (index==4) sfx=10.;
    if (index==7) {sfx=0.7;}
    if (index==8) sfx=2.5;
    if (index==11) {sfx=0.7;}
    if (index==12) sfx=2.;
    if (index==15) {sfx=0.7;}
    if (index==19) {sfx=0.5;}
    if (index==23) {sfx=0.5;}
    if (index==27) {sfx=0.5;}
    if (index==31) {sfx=0.5;}
    if (index==35) {sfx=0.5;}
    if (index==39) {sfx=0.5;}
    if (index==40) sfx=2.;
    if (index==44) sfx=4.;
    if (index==48) sfx=15.;
    if (index==55) {sfx=0.7;}
    if (index==59) {sfx=0.5;}
    if (index==63) {sfx=0.5;sfy=2.;}
    if (index==67) {sfx=0.4;sfy=2.;}
    if (index==68) {sfy=1.2;}
    if (index==71) {sfx=0.35;sfy=2.5;}
    if (index==72) {sfy=1.2;}
    if (index==75) {sfx=0.7;sfy=2.;}
    
    
    if (index<40)
    {
        int fi=(int)(index/4);
        int img=index-fi*4;
        dai(fi,img,kc,g,ks);
        cout <<"Dai "<<index<<" fi "<<fi<<" img "<<img<<" kc "<<kc<<" g "<<g<<" ks "<<ks<<endl;
    }
    else
    {
        index-=40;
        int gi=(int)(index/4);
        int img=index-gi*4;
        blackburne(gi,img,kc,g,ks);
        cout <<"Bla "<<index<<" gi "<<gi<<" img "<<img<<" kc "<<kc<<" g "<<g<<" ks "<<ks<<endl;
    }
}

void dai(int fi,int img, double &kc, double &g, double &ks)
// fi: f_* index, 0...9, img: image index, 0.,... 3
{
    double Table1[40][4]={
        {1, 0.667, 0.359, 0.030},
        {2, 0.631, 0.325, 0.027},
        {3, 0.644, 0.306, 0.028},
        {4, 1.079, 0.493, 0.079},
        {1, 0.618, 0.412, 0.067},
        {2, 0.581, 0.367, 0.060},
        {3, 0.595, 0.346, 0.062},
        {4, 1.041, 0.631, 0.159},
        {1, 0.569, 0.465, 0.110},
        {2, 0.530, 0.410, 0.099},
        {3, 0.546, 0.387, 0.103},
        {4, 1.001, 0.635, 0.242},
        {1, 0.519, 0.518, 0.162},
        {2, 0.480, 0.453, 0.146},
        {3, 0.496, 0.427, 0.153},
        {4, 0.964, 0.895, 0.329},
        {1, 0.469, 0.572, 0.226},
        {2, 0.430, 0.497, 0.204},
        {3, 0.447, 0.469, 0.214},
        {4, 0.925, 1.018, 0.421},
        {1, 0.419, 0.626, 0.305},
        {2, 0.379, 0.540, 0.278},
        {3, 0.397, 0.511, 0.290},
        {4, 0.890, 1.139, 0.520},
        {1, 0.369, 0.679, 0.406},
        {2, 0.329, 0.584, 0.375},
        {3, 0.348, 0.552, 0.390},
        {4, 0.851, 1.288, 0.625},
        {1, 0.318, 0.734, 0.541},
        {2, 0.278, 0.628, 0.507},
        {3, 0.297, 0.595, 0.524},
        {4, 0.816, 1.412, 0.740},
        {1, 0.268, 0.787, 0.725},
        {2, 0.228, 0.671, 0.697},
        {3, 0.247, 0.637, 0.711},
        {4, 0.781, 1.530, 0.863},
        {1, 0.217, 0.842, 1.000},
        {2, 0.178, 0.714, 1.000},
        {3, 0.196, 0.679, 1.000},
        {4, 0.740, 1.639, 1.000}};
    
    int i=fi*4+img;
    ks = Table1[i][1]*Table1[i][3];
    g  = Table1[i][2];
    kc = Table1[i][1]-ks;
}

void blackburne(int gi,int img, double &kc, double &g, double &ks)
// gi, g_* index, 0...,8
{
    double Table1[4][3]={{1,0.442,0.597},{2,0.423,0.507},{3,0.422,0.504},{4,0.834,0.989}};
    double Table2[9]={0.025,0.05,0.1,0.25,0.5,0.75,0.9,0.95,0.975};
    
    kc=Table1[img][1];
    g=Table1[img][2];
    ks=Table2[gi]*kc;
    kc-=ks;
}