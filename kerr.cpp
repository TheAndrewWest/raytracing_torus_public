#include "kerr.h"
#include "metric.h"
#include "xTrackMath.h"

#include <cmath>
#include <iostream>


void KerrMetric::Initialize(double hair)
{
  ::CalculateMetricElements = &KerrMetric::CalculateMetricElements;
  ::CalculateMetricElements3 = &KerrMetric::CalculateMetricElements3;
  if (hair)
    ::CalculateChristoffelSymbols = &KerrMetric::CalculateChristoffelSymbols;
  else
    ::CalculateChristoffelSymbols = &KerrMetric::CalculateChristoffelSymbolsNoHair;
}
  

void KerrMetric::CalculateChristoffelSymbols(double M, double a, double hair, const double *y, std::vector<double> &symbols)
{
  const double a_2 = a*a;
  const double a_3 = a_2*a;
  const double a_4 = a_3*a;
  const double a_6 = a_4*a_2;
  const double a_8 = a_6*a_2;
  const double M_2 = M*M;
  const double M_3 = M_2*M;
  const double M_4 = M_3*M;
  const double M_6 = M_3*M_3;
  const double M_7 = M_6*M;
  const double hair_2 = hair*hair;
  const double y1 = y[1];
  const double y1_2 = y1*y1;
  const double y1_3 = y1_2*y1;
  const double y1_4 = y1_3*y1;
  const double y1_5 = y1_4*y1;
  const double y1_6 = y1_5*y1;
  const double y1_7 = y1_6*y1;
  const double y1_8 = y1_7*y1;
  const double y1_9 = y1_8*y1;
  const double y2 = y[2];
  const double sin_y2 = sin(y2);
  const double sin_y2_2 = sin_y2*sin_y2;
  const double sin_y2_3 = sin_y2_2*sin_y2;
  const double sin_y2_4 = sin_y2_3*sin_y2;
  const double cos_y2 = cos(y2);
  const double cos_y2_2 = cos_y2*cos_y2;
  const double cos_y2_3 = cos_y2_2*cos_y2;
  const double cos_y2_4 = cos_y2_3*cos_y2;
  const double cos_y2_6 = cos_y2_4*cos_y2_2;
  const double cos_y2_8 = cos_y2_6*cos_y2_2;
  const double cos_y2_10 = cos_y2_8*cos_y2_2;
  const double cos_2y2 = cos_y2_2 - sin_y2_2;     // cos(2x) = cos^2(x) - sin^2(x)
  const double cos_4y2 = 2*cos_2y2*cos_2y2 - 1;   // cos(4x) = 2cos^2(2x) - 1
  const double cot_y2 = cos_y2/sin_y2;
  const double sin_2y2 = 2*sin_y2*cos_y2;         // sin(2x) = 2*sin(x)*cos(x)
  const double sin_2y2_2 = sqr(sin_2y2);
  const double pow2_y2_2_p_a_2cos_y2_2 = sqr(y1_2 + a_2*cos_y2_2);
  const double _y1_2_p_a_2cos_y2_2 = y1_2 + a_2*cos_y2_2;
  const double pow2_y1_2_p_a_2cos_y2_2 = sqr(_y1_2_p_a_2cos_y2_2);
  const double pow3_y1_2_p_a_2cos_y2_2 = pow2_y1_2_p_a_2cos_y2_2*_y1_2_p_a_2cos_y2_2;
  const double pow4_y1_2_p_a_2cos_y2_2 = pow3_y1_2_p_a_2cos_y2_2*_y1_2_p_a_2cos_y2_2;
  const double pow5_y1_2_p_a_2cos_y2_2 = pow4_y1_2_p_a_2cos_y2_2*_y1_2_p_a_2cos_y2_2;
  const double _2y1_2_p_a_2_p_a_2cos_2y2 = 2*y1_2 + a_2 + a_2*cos_2y2;
  const double pow2_2y1_2_p_a_2_p_a_2cos_2y2 = sqr(_2y1_2_p_a_2_p_a_2cos_2y2);
  const double pow3_2y1_2_p_a_2_p_a_2cos_2y2 = pow2_2y1_2_p_a_2_p_a_2cos_2y2*_2y1_2_p_a_2_p_a_2cos_2y2;
  const double pow4_2y1_2_p_a_2_p_a_2cos_2y2 = pow3_2y1_2_p_a_2_p_a_2cos_2y2*_2y1_2_p_a_2_p_a_2cos_2y2;


  const double s2_8_enumerator = (y1*a_2*M*cos_y2*sin_y2*(2*a_6*(-4*y1_2*(a_2 + y1*(y1 - 2*M)) + (y1_2 + a_2)*hair*M_2)*cos_y2_6 - 2*a_8*(a_2 + y1*(y1 - 2*M))*cos_y2_8 - y1*a_4*cos_y2_4*(12*y1_3*(y1_2 + a_2 - 2*y1*M) + hair*M_2*(-6*y1_3 - 6*y1*a_2 + 6*y1_2*M + 5*a_2*M - 8*y1*M_2) + a_2*hair*M_3*cos_2y2) + 2*y1*a_2*cos_y2_2*(-4*y1_5*(a_2 + y1*(y1 - 2*M)) + y1_2*hair*M_2*(3*a_2*(y1 - 2*M) + y1*(3*y1_2 - 6*y1*M + 8*M_2)) + a_2*hair*M_3*(2*y1_2 + hair*M_2)*sin_y2_2) + 2*y1_2*(-(y1_6*(a_2 + y1*(y1 - 2*M))) + 2*y1*hair_2*M_7 + y1_3*hair*M_2*(a_2*(y1 - 3*M) + y1*(y1_2 - 3*y1*M + 4*M_2)) + a_2*hair*M_3*(y1_3 + hair*(y1 - M)*M_2)*sin_y2_2)));

  const double s1_8_denominator = pow2_y2_2_p_a_2cos_y2_2*(y1_4 + y1*hair*M_3 + 2*y1_2*a_2*cos_y2_2 + a_4*cos_y2_4)*(-(y1_3*(y1_2 + a_2)*(-y1 + 2*M)) + a_2*(a_2*(y1_2 + a_2)*cos_y2_4 - y1*cos_y2_2*(a_2*(-2*y1 + M) + 2*y1_2*(-y1 + M) + a_2*M*cos_2y2) + y1*M*(2*y1_2 + hair*M_2)*sin_y2_2));

  const double s2_7_11_denominator = (pow2_y1_2_p_a_2cos_y2_2*(y1_4 + y1*hair*M_3 + 2*y1_2*a_2*cos_y2_2 + a_4*cos_y2_4)*(-(y1_3*(y1_2 + a_2)*(-y1 + 2*M)) + a_2*(a_2*(y1_2 + a_2)*cos_y2_4 - y1*cos_y2_2*(a_2*(-2*y1 + M) + 2*y1_2*(-y1 + M) + a_2*M*cos_2y2) + y1*M*(2*y1_2 + hair*M_2)*sin_y2_2)));
      
  symbols[1] = symbols[4] =
    -(M*((y1_2 + a_2)*pow2_y2_2_p_a_2cos_y2_2*(-2*y1_6 + y1_3*hair*(3*y1 - 8*M)*M_2 + 2*y1*a_2*(-y1_3 + hair*M_2*(y1 + 2*M))*cos_y2_2 + a_4*(2*y1_2 - hair*M_2)*cos_y2_4 + 2*a_6*cos_y2_6) + y1*a_2*hair*M_3*(2*y1*a_2*hair*M_2*(y1 + M)*cos_y2_2 + a_4*(4*y1_2 - hair*M_2)*cos_y2_4 + y1_3*(4*y1*(y1_2 + a_2) + hair*(3*y1 - 2*M)*M_2 + 4*y1*a_2*cos_2y2))*sin_y2_2))/(2.*s1_8_denominator);

  symbols[2] = s2_8_enumerator/s2_7_11_denominator;

  symbols[7] = symbols[13] =
    (a*M*sin_y2_2*(-(y1_7*(3*y1_2*(y1_3 + 2*hair*M_3) + a_2*(y1_3 + 4*hair*M_3))) + a_2*(2*y1*a_4*(-9*y1_5 + a_2*(y1_3 + hair*M_3))*cos_y2_6 + y1_2*a_6*(-7*y1_2 + 3*a_2)*cos_y2_8 + a_8*(-y1 + a)*(y1 + a)*cos_y2_10 - 2*y1_3*a_2*cos_y2_4*(y1_2*(11*y1_3 + y1*a_2 + 3*hair*M_3) - a_2*hair*M_3*sin_y2_2) - y1_2*cos_y2_2*(13*y1_8 + 3*y1_6*a_2 + 6*y1_3*(2*y1_2 + a_2)*hair*M_3 - a_2*hair_2*M_6*sin_y2_2) + y1_4*hair*M_3*((2*y1_3 - hair*M_3)*sin_y2_2 + y1*a_2*sin_2y2_2))))/s2_7_11_denominator;

  symbols[8] = s2_8_enumerator/s1_8_denominator;

  symbols[11] = symbols[14] =
    (y1*a_3*M*cos_y2*sin_y2_3*(2*y1*a_6*(4*y1*(a_2 + y1*(y1 - 2*M)) - hair*M_3)*cos_y2_6 + 2*a_8*(a_2 + y1*(y1 - 2*M))*cos_y2_8 + y1*a_4*cos_y2_4*(hair*M_3*(5*a_2 - 8*y1*M) + 12*y1_3*(y1_2 + a_2 - 2*y1*M) + a_2*hair*M_3*cos_2y2) + 2*y1_2*a_2*cos_y2_2*(-((y1_3 + hair*M_3)*(-4*y1_3 + 8*y1_2*M + hair*M_3)) + a_2*(4*y1_4 + 5*y1*hair*M_3) + y1*a_2*hair*M_3*cos_2y2) + 2*y1_2*(y1_3*hair*(3*a_2 + 2*y1*(y1 - 2*M))*M_3 - y1*hair_2*M_6*(y1 + 2*M) + y1_6*(y1_2 + a_2 - 2*y1*M) + a_2*hair*M_3*(-y1_3 + hair*M_3)*sin_y2_2)))/s2_7_11_denominator;

  symbols[16] = (M*(y1_3*(2*y1_3 - 3*y1*hair*M_2 + 8*hair*M_3) + 2*y1*a_2*(y1_3 - hair*M_2*(y1 + 2*M))*cos_y2_2 + a_4*(-2*y1_2 + hair*M_2)*cos_y2_4 - 2*a_6*cos_y2_6)*((a_2 + y1*(y1 - 2*M))*pow2_y1_2_p_a_2cos_y2_2 + y1*a_2*hair*M_3*sin_y2_2))/(2.*pow5_y1_2_p_a_2cos_y2_2*(y1_4 + y1*hair*M_3 + 2*y1_2*a_2*cos_y2_2 + a_4*cos_y2_4));

  symbols[19] = symbols[28] =
    (a*M*(-(y1_3*(y1_3 + 4*hair*M_3)) - y1*a_2*(y1_3 - 2*hair*M_3)*cos_y2_2 + y1_2*a_4*cos_y2_4 + a_6*cos_y2_6)*sin_y2_2*((a_2 + y1*(y1 - 2*M))*pow2_y1_2_p_a_2cos_y2_2 + y1*a_2*hair*M_3*sin_y2_2))/(pow5_y1_2_p_a_2cos_y2_2*(y1_4 + y1*hair*M_3 + 2*y1_2*a_2*cos_y2_2 + a_4*cos_y2_4));

  symbols[21] = (5*y1_2 - a_2*cos_y2_2 + (y1*hair*M_3*(-3*y1_2 + a_2*cos_y2_2))/(y1_4 + y1*hair*M_3 + 2*y1_2*a_2*cos_y2_2 + a_4*cos_y2_4) + (pow2_y2_2_p_a_2cos_y2_2*(y1_2*(-3*a_2 + y1*(-5*y1 + 8*M)) + a_2*(-y1 + a)*(y1 + a)*cos_y2_2))/((a_2 + y1*(y1 - 2*M))*pow2_y2_2_p_a_2cos_y2_2 + y1*a_2*hair*M_3*sin_y2_2))/(2.*(y1_3 + y1*a_2*cos_y2_2));

  symbols[22] = symbols[25] =
    a_2*cos_y2*sin_y2*(-(1/(y1_2 + a_2*cos_y2_2)) - (2*(y1_2 + a_2*cos_y2_2))/(y1_4 + y1*hair*M_3 + 2*y1_2*a_2*cos_y2_2 + a_4*cos_y2_4) + (y1*(2*y1*(a_2 + y1*(y1 - 2*M)) - hair*M_3) + 2*a_2*(a_2 + y1*(y1 - 2*M))*cos_y2_2)/((a_2 + y1*(y1 - 2*M))*pow2_y1_2_p_a_2cos_y2_2 + y1*a_2*hair*M_3*sin_y2_2));

  symbols[26] = -((y1*(a_2 + y1*(y1 - 2*M) + (y1*a_2*hair*M_3*sin_y2_2)/pow2_y1_2_p_a_2cos_y2_2))/(a_2*cos_y2_2 + y1*(y1 + (hair*M_3)/(y1_2 + a_2*cos_y2_2))));

  symbols[31] = -(sin_y2_2*((a_2 + y1*(y1 - 2*M))*pow2_y1_2_p_a_2cos_y2_2 + y1*a_2*hair*M_3*sin_y2_2)*(2*y1_9 + a_2*(2*y1*a_6*cos_y2_8 - y1_3*M*(2*y1_3 + hair*M_2*(3*y1 + 8*M))*sin_y2_2 + 2*cos_y2_6*(4*y1_3*a_4 + a_6*M*sin_y2_2) + 2*y1_2*cos_y2_2*(4*y1_5 - a_2*M*(y1_2 + hair*M_2)*sin_y2_2) + a_2*cos_y2_4*(12*y1_5 + a_2*M*(2*y1_2 + hair*M_2)*sin_y2_2) + y1*a_2*hair*M_4*sin_2y2_2)))/(2.*pow5_y1_2_p_a_2cos_y2_2*(y1_4 + y1*hair*M_3 + 2*y1_2*a_2*cos_y2_2 + a_4*cos_y2_4));

  symbols[32] = (-2*y1*a_2*M*cos_y2*(y1_4 + y1*hair*M_2*(-y1 + 3*M) + a_2*(2*y1_2 - hair*M_2)*cos_y2_2 + a_4*cos_y2_4)*sin_y2)/pow5_y1_2_p_a_2cos_y2_2;

  symbols[35] = symbols[44] =
    (y1*a*M*(8*y1_6 + 16*y1_4*a_2 + 11*y1_2*a_4 + 3*a_6 + 8*y1*(y1_2 + 2*a_2)*hair*M_3 + 4*a_2*(2*y1_4 + 3*y1_2*a_2 + a_4 - 2*y1*hair*M_3)*cos_2y2 + a_4*(y1_2 + a_2)*cos_4y2)*sin_2y2)/(8.*pow5_y1_2_p_a_2cos_y2_2);

  symbols[37] = (a_2*cos_y2*(y1_2 + a_2 - 2*y1*M + (16*y1_2*(y1_2 + a_2)*hair_2*M_6)/pow4_2y1_2_p_a_2_p_a_2cos_2y2 + (8*y1*hair*M_3*(a_2 + y1*(y1 + M)))/pow2_2y1_2_p_a_2_p_a_2cos_2y2 - (4*y1*hair*M_3)/(2*y1_2 + a_2 + a_2*cos_2y2))*sin_y2)/((y1_2 + a_2*cos_y2_2)*sqr(a_2 + y1*(y1 - 2*M) + (y1*a_2*hair*M_3*sin_y2_2)/pow2_y1_2_p_a_2cos_y2_2));

  symbols[38] = symbols[41] =
    y1/(y1_2 + a_2*cos_y2_2);

  symbols[42] = -((a_2*cos_y2*sin_y2)/(y1_2 + a_2*cos_y2_2));

  symbols[47] = -((cos_y2*sin_y2*((y1_2 + a_2)*pow4_y1_2_p_a_2cos_y2_2 + y1*a_2*M*(4*y1_6 + 7*y1_4*a_2 + y1*hair*M_2*(2*y1_2*(y1 + 2*M) + a_2*(3*y1 + 5*M)) + 2*a_4*(6*y1_2 + hair*M_2)*cos_y2_4 + 4*a_6*cos_y2_6 + y1*a_2*(5*y1_3 + hair*(y1 - M)*M_2)*cos_2y2)*sin_y2_2 + 2*y1*a_6*M*cos_y2_2*(2*y1_2 + hair*M_2 + a_2*cos_y2_2)*sin_y2_4))/pow5_y1_2_p_a_2cos_y2_2);

  symbols[49] = symbols[52] =
    (a*M*(y1_2 - a_2*cos_y2_2)*(y1_4 + y1*hair*M_3 + 2*y1_2*a_2*cos_y2_2 + a_4*cos_y2_4))/(pow2_y1_2_p_a_2cos_y2_2*(-(y1_3*(y1_2 + a_2)*(-y1 + 2*M)) + a_2*(a_2*(y1_2 + a_2)*cos_y2_4 - y1*cos_y2_2*(a_2*(-2*y1 + M) + 2*y1_2*(-y1 + M) + a_2*M*cos_2y2) + y1*M*(2*y1_2 + hair*M_2)*sin_y2_2)));

  symbols[50] = symbols[56] =
    -(y1*a*(a_2 + y1*(y1 - 2*M))*M*(2*y1_2 + a_2 + a_2*cos_2y2)*(8*y1_4 + 8*y1_2*a_2 + 3*a_4 + 8*y1*hair*M_3 + 4*a_2*(2*y1_2 + a_2)*cos_2y2 + a_4*cos_4y2)*cot_y2)/(8.*pow3_y1_2_p_a_2cos_y2_2*(-(y1_3*(y1_2 + a_2)*(-y1 + 2*M)) + a_2*(a_2*(y1_2 + a_2)*cos_y2_4 - y1*cos_y2_2*(a_2*(-2*y1 + M) + 2*y1_2*(-y1 + M) + a_2*M*cos_2y2) + y1*M*(2*y1_2 + hair*M_2)*sin_y2_2)));

  symbols[55] = symbols[61] =
    (2*y1*pow3_y1_2_p_a_2cos_y2_2*(y1*(y1 - 2*M) + a_2*cos_y2_2) + a_2*M*(-2*y1_6 - y1_3*hair*M_2*(3*y1 + 2*M) - 2*y1*a_2*(y1_3 + hair*(y1 - M)*M_2)*cos_y2_2 + a_4*(2*y1_2 + hair*M_2)*cos_y2_4 + 2*a_6*cos_y2_6)*sin_y2_2)/(2.*pow2_y1_2_p_a_2cos_y2_2*(-(y1_3*(y1_2 + a_2)*(-y1 + 2*M)) + a_2*(a_2*(y1_2 + a_2)*cos_y2_4 - y1*cos_y2_2*(a_2*(-2*y1 + M) + 2*y1_2*(-y1 + M) + a_2*M*cos_2y2) + y1*M*(2*y1_2 + hair*M_2)*sin_y2_2)));

  symbols[59] = symbols[62] =
    (((y1_2 + a_2)*pow3_2y1_2_p_a_2_p_a_2cos_2y2*(a_2 + 2*y1*(y1 - 2*M) + a_2*cos_2y2)*cot_y2)/16. + y1*a_2*M*((a_2*cos_y2_3*(7*a_4 + 16*y1_2*(3*y1_2 - 2*y1*M + hair*M_2) + 8*a_2*(4*y1_2 - y1*M + hair*M_2) + 8*a_2*(a_2 + y1*(2*y1 - M))*cos_2y2 + a_4*cos_4y2)*sin_y2)/4. + y1*(2*y1_5 - 2*y1_4*M + y1_3*hair*M_2 - 2*y1*hair*M_4 + a_2*(y1_3 + hair*M_2*(y1 + M))*sin_y2_2)*sin_2y2))/(pow2_y1_2_p_a_2cos_y2_2*(-(y1_3*(y1_2 + a_2)*(-y1 + 2*M)) + a_2*(a_2*(y1_2 + a_2)*cos_y2_4 - y1*cos_y2_2*(a_2*(-2*y1 + M) + 2*y1_2*(-y1 + M) + a_2*M*cos_2y2) + y1*M*(2*y1_2 + hair*M_2)*sin_y2_2)));
}


void KerrMetric::CalculateChristoffelSymbolsNoHair(double M, double a, double, const double *y, std::vector<double> &symbols)
{
  const double a_2 = a*a;
  const double a_3 = a_2*a;
  const double a_4 = a_3*a;
  const double a_6 = a_4*a_2;
  const double a_8 = a_6*a_2;
  const double y1 = y[1];
  const double y1_2 = y1*y1;
  const double y1_3 = y1_2*y1;
  const double y1_4 = y1_3*y1;
  const double y1_5 = y1_4*y1;
  const double y1_6 = y1_5*y1;
  const double y1_7 = y1_6*y1;
  const double y1_8 = y1_7*y1;
  const double y1_9 = y1_8*y1;
  const double y2 = y[2];
  const double sin_y2 = sin(y2);
  const double sin_y2_2 = sin_y2*sin_y2;
  const double sin_y2_3 = sin_y2_2*sin_y2;
  const double sin_y2_4 = sin_y2_3*sin_y2;
  const double cos_y2 = cos(y2);
  const double cos_y2_2 = cos_y2*cos_y2;
  const double cos_y2_3 = cos_y2_2*cos_y2;
  const double cos_y2_4 = cos_y2_3*cos_y2;
  const double cos_y2_6 = cos_y2_4*cos_y2_2;
  const double cos_y2_8 = cos_y2_6*cos_y2_2;
  const double cos_y2_10 = cos_y2_8*cos_y2_2;
  const double cos_2y2 = cos_y2_2 - sin_y2_2;     // cos(2x) = cos^2(x) - sin^2(x)
  const double cos_4y2 = 2*cos_2y2*cos_2y2 - 1;   // cos(4x) = 2cos^2(2x) - 1
  const double cot_y2 = cos_y2/sin_y2;
  const double sin_2y2 = 2*sin_y2*cos_y2;         // sin(2x) = 2*sin(x)*cos(x)
  const double pow2_y2_2_p_a_2cos_y2_2 = sqr(y1_2 + a_2*cos_y2_2);
  const double _y1_2_p_a_2cos_y2_2 = y1_2 + a_2*cos_y2_2;
  const double pow2_y1_2_p_a_2cos_y2_2 = sqr(_y1_2_p_a_2cos_y2_2);
  const double pow3_y1_2_p_a_2cos_y2_2 = pow2_y1_2_p_a_2cos_y2_2*_y1_2_p_a_2cos_y2_2;
  const double pow4_y1_2_p_a_2cos_y2_2 = pow3_y1_2_p_a_2cos_y2_2*_y1_2_p_a_2cos_y2_2;
  const double pow5_y1_2_p_a_2cos_y2_2 = pow4_y1_2_p_a_2cos_y2_2*_y1_2_p_a_2cos_y2_2;
  const double _2y1_2_p_a_2_p_a_2cos_2y2 = 2*y1_2 + a_2 + a_2*cos_2y2;
  const double pow2_2y1_2_p_a_2_p_a_2cos_2y2 = sqr(_2y1_2_p_a_2_p_a_2cos_2y2);
  const double pow3_2y1_2_p_a_2_p_a_2cos_2y2 = pow2_2y1_2_p_a_2_p_a_2cos_2y2*_2y1_2_p_a_2_p_a_2cos_2y2;

  const double s2_8_enumerator = (y1*a_2*M*cos_y2*sin_y2*(2*a_6*(-4*y1_2*(a_2 + y1*(y1 - 2*M)))*cos_y2_6 - 2*a_8*(a_2 + y1*(y1 - 2*M))*cos_y2_8 - y1*a_4*cos_y2_4*(12*y1_3*(y1_2 + a_2 - 2*y1*M)) + 2*y1*a_2*cos_y2_2*(-4*y1_5*(a_2 + y1*(y1 - 2*M))) + 2*y1_2*(-(y1_6*(a_2 + y1*(y1 - 2*M))))));
  
  const double s2_7_11_denominator = pow2_y1_2_p_a_2cos_y2_2*(y1_4 + 2*y1_2*a_2*cos_y2_2 + a_4*cos_y2_4)*(-(y1_3*(y1_2 + a_2)*(-y1 + 2*M)) + a_2*(a_2*(y1_2 + a_2)*cos_y2_4 - y1*cos_y2_2*(a_2*(-2*y1 + M) + 2*y1_2*(-y1 + M) + a_2*M*cos_2y2) + M*2*y1_3*sin_y2_2));
    
  symbols[1] = symbols[4] =
    -(M*((y1_2 + a_2)*pow2_y2_2_p_a_2cos_y2_2*(-2*y1_6 + 2*y1*a_2*(-y1_3)*cos_y2_2 + a_4*2*y1_2*cos_y2_4 + 2*a_6*cos_y2_6)))/(2.*pow2_y2_2_p_a_2cos_y2_2*(y1_4 + 2*y1_2*a_2*cos_y2_2 + a_4*cos_y2_4)*(-(y1_3*(y1_2 + a_2)*(-y1 + 2*M)) + a_2*(a_2*(y1_2 + a_2)*cos_y2_4 - y1*cos_y2_2*(a_2*(-2*y1 + M) + 2*y1_2*(-y1 + M) + a_2*M*cos_2y2) + M*2*y1_3*sin_y2_2)));

  symbols[2] = s2_8_enumerator/s2_7_11_denominator;

  symbols[7] = symbols[13] =
    (a*M*sin_y2_2*(-(y1_7*(3*y1_2*(y1_3) + a_2*(y1_3))) + a_2*(2*y1*a_4*(-9*y1_5 + a_2*(y1_3))*cos_y2_6 + y1_2*a_6*(-7*y1_2 + 3*a_2)*cos_y2_8 + a_8*(-y1 + a)*(y1 + a)*cos_y2_10 - 2*y1_3*a_2*cos_y2_4*(y1_2*(11*y1_3 + y1*a_2)) - y1_2*cos_y2_2*(13*y1_8 + 3*y1_6*a_2))))/s2_7_11_denominator;

  symbols[8] = s2_8_enumerator/(pow2_y2_2_p_a_2cos_y2_2*(y1_4 + 2*y1_2*a_2*cos_y2_2 + a_4*cos_y2_4)*(-(y1_3*(y1_2 + a_2)*(-y1 + 2*M)) + a_2*(a_2*(y1_2 + a_2)*cos_y2_4 - y1*cos_y2_2*(a_2*(-2*y1 + M) + 2*y1_2*(-y1 + M) + a_2*M*cos_2y2) + y1*M*(2*y1_2)*sin_y2_2)));

  symbols[11] = symbols[14] =
    (y1*a_3*M*cos_y2*sin_y2_3*(2*y1*a_6*(4*y1*(a_2 + y1*(y1 - 2*M)))*cos_y2_6 + 2*a_8*(a_2 + y1*(y1 - 2*M))*cos_y2_8 + y1*a_4*cos_y2_4*(12*y1_3*(y1_2 + a_2 - 2*y1*M)) + 2*y1_2*a_2*cos_y2_2*(-((y1_3)*(-4*y1_3 + 8*y1_2*M)) + a_2*(4*y1_4)) + 2*y1_2*(y1_6*(y1_2 + a_2 - 2*y1*M))))/s2_7_11_denominator;

  symbols[16] = (M*(y1_3*2*y1_3 + 2*y1*a_2*y1_3*cos_y2_2 + a_4*(-2*y1_2)*cos_y2_4 - 2*a_6*cos_y2_6)*((a_2 + y1*(y1 - 2*M))*pow2_y1_2_p_a_2cos_y2_2))/(2.*pow5_y1_2_p_a_2cos_y2_2*(y1_4 + 2*y1_2*a_2*cos_y2_2 + a_4*cos_y2_4));

  symbols[19] = symbols[28] =
    (a*M*(-(y1_3*y1_3) - y1*a_2*y1_3*cos_y2_2 + y1_2*a_4*cos_y2_4 + a_6*cos_y2_6)*sin_y2_2*((a_2 + y1*(y1 - 2*M))*pow2_y1_2_p_a_2cos_y2_2))/(pow5_y1_2_p_a_2cos_y2_2*(y1_4 + 2*y1_2*a_2*cos_y2_2 + a_4*cos_y2_4));

  symbols[21] = (5*y1_2 - a_2*cos_y2_2 + (pow2_y2_2_p_a_2cos_y2_2*(y1_2*(-3*a_2 + y1*(-5*y1 + 8*M)) + a_2*(-y1 + a)*(y1 + a)*cos_y2_2))/((a_2 + y1*(y1 - 2*M))*pow2_y2_2_p_a_2cos_y2_2))/(2.*(y1_3 + y1*a_2*cos_y2_2));

  symbols[22] = symbols[25] =
    a_2*cos_y2*sin_y2*(-(1/(y1_2 + a_2*cos_y2_2)) - (2*(y1_2 + a_2*cos_y2_2))/(y1_4 + 2*y1_2*a_2*cos_y2_2 + a_4*cos_y2_4) + (y1*(2*y1*(a_2 + y1*(y1 - 2*M))) + 2*a_2*(a_2 + y1*(y1 - 2*M))*cos_y2_2)/((a_2 + y1*(y1 - 2*M))*pow2_y1_2_p_a_2cos_y2_2));

  symbols[26] = -(y1*(a_2 + y1*(y1 - 2*M)))/(a_2*cos_y2_2 + y1*y1);

  symbols[31] = -(sin_y2_2*((a_2 + y1*(y1 - 2*M))*pow2_y1_2_p_a_2cos_y2_2)*(2*y1_9 + a_2*(2*y1*a_6*cos_y2_8 - y1_3*M*2*y1_3*sin_y2_2 + 2*cos_y2_6*(4*y1_3*a_4 + a_6*M*sin_y2_2) + 2*y1_2*cos_y2_2*(4*y1_5 - a_2*M*y1_2*sin_y2_2) + a_2*cos_y2_4*(12*y1_5 + a_2*M*2*y1_2*sin_y2_2))))/(2.*pow5_y1_2_p_a_2cos_y2_2*(y1_4 + 2*y1_2*a_2*cos_y2_2 + a_4*cos_y2_4));

  symbols[32] = (-2*y1*a_2*M*cos_y2*(y1_4 + a_2*2*y1_2*cos_y2_2 + a_4*cos_y2_4)*sin_y2)/pow5_y1_2_p_a_2cos_y2_2;

  symbols[35] = symbols[44] =
    (y1*a*M*(8*y1_6 + 16*y1_4*a_2 + 11*y1_2*a_4 + 3*a_6 + 4*a_2*(2*y1_4 + 3*y1_2*a_2 + a_4)*cos_2y2 + a_4*(y1_2 + a_2)*cos_4y2)*sin_2y2)/(8.*pow5_y1_2_p_a_2cos_y2_2);

  symbols[37] = (a_2*cos_y2*(y1_2 + a_2 - 2*y1*M)*sin_y2)/((y1_2 + a_2*cos_y2_2)*sqr(a_2 + y1*(y1 - 2*M)));

  symbols[38] = symbols[41] =
    y1/(y1_2 + a_2*cos_y2_2);

  symbols[42] = -((a_2*cos_y2*sin_y2)/(y1_2 + a_2*cos_y2_2));

  symbols[47] = -((cos_y2*sin_y2*((y1_2 + a_2)*pow4_y1_2_p_a_2cos_y2_2 + y1*a_2*M*(4*y1_6 + 7*y1_4*a_2 + 12*a_4*y1_2*cos_y2_4 + 4*a_6*cos_y2_6 + y1*a_2*5*y1_3*cos_2y2)*sin_y2_2 + 2*y1*a_6*M*cos_y2_2*(2*y1_2 + a_2*cos_y2_2)*sin_y2_4))/pow5_y1_2_p_a_2cos_y2_2);

  symbols[49] = symbols[52] =
    (a*M*(y1_2 - a_2*cos_y2_2)*(y1_4 + 2*y1_2*a_2*cos_y2_2 + a_4*cos_y2_4))/(pow2_y1_2_p_a_2cos_y2_2*(-(y1_3*(y1_2 + a_2)*(-y1 + 2*M)) + a_2*(a_2*(y1_2 + a_2)*cos_y2_4 - y1*cos_y2_2*(a_2*(-2*y1 + M) + 2*y1_2*(-y1 + M) + a_2*M*cos_2y2) + y1*M*2*y1_2*sin_y2_2)));

  symbols[50] = symbols[56] =
    -(y1*a*(a_2 + y1*(y1 - 2*M))*M*(2*y1_2 + a_2 + a_2*cos_2y2)*(8*y1_4 + 8*y1_2*a_2 + 3*a_4 + 4*a_2*(2*y1_2 + a_2)*cos_2y2 + a_4*cos_4y2)*cot_y2)/(8.*pow3_y1_2_p_a_2cos_y2_2*(-(y1_3*(y1_2 + a_2)*(-y1 + 2*M)) + a_2*(a_2*(y1_2 + a_2)*cos_y2_4 - y1*cos_y2_2*(a_2*(-2*y1 + M) + 2*y1_2*(-y1 + M) + a_2*M*cos_2y2) + y1*M*2*y1_2*sin_y2_2)));

  symbols[55] = symbols[61] =
    (2*y1*pow3_y1_2_p_a_2cos_y2_2*(y1*(y1 - 2*M) + a_2*cos_y2_2) + a_2*M*(-2*y1_6 - 2*a_2*y1_4*cos_y2_2 + a_4*2*y1_2*cos_y2_4 + 2*a_6*cos_y2_6)*sin_y2_2)/(2.*pow2_y1_2_p_a_2cos_y2_2*(-(y1_3*(y1_2 + a_2)*(-y1 + 2*M)) + a_2*(a_2*(y1_2 + a_2)*cos_y2_4 - y1*cos_y2_2*(a_2*(-2*y1 + M) + 2*y1_2*(-y1 + M) + a_2*M*cos_2y2) + M*2*y1_3*sin_y2_2)));

  symbols[59] = symbols[62] =
    (((y1_2 + a_2)*pow3_2y1_2_p_a_2_p_a_2cos_2y2*(a_2 + 2*y1*(y1 - 2*M) + a_2*cos_2y2)*cot_y2)/16. + y1*a_2*M*((a_2*cos_y2_3*(7*a_4 + 16*y1_2*(3*y1_2 - 2*y1*M) + 8*a_2*(4*y1_2 - y1*M) + 8*a_2*(a_2 + y1*(2*y1 - M))*cos_2y2 + a_4*cos_4y2)*sin_y2)/4. + y1*(2*y1_5 - 2*y1_4*M + a_2*y1_3*sin_y2_2)*sin_2y2))/(pow2_y1_2_p_a_2cos_y2_2*(-(y1_3*(y1_2 + a_2)*(-y1 + 2*M)) + a_2*(a_2*(y1_2 + a_2)*cos_y2_4 - y1*cos_y2_2*(a_2*(-2*y1 + M) + 2*y1_2*(-y1 + M) + a_2*M*cos_2y2) + M*2*y1_3*sin_y2_2)));
}


void KerrMetric::CalculateMetricElements(double M, double a, double hair, const double *y, std::vector<double> &metric)
{
  const double a_2 = sqr(a);
  const double y1_2 = sqr(y[1]);
  const double cos_y2_2 = sqr(cos(y[2]));
  const double sin_y2_2 = 1 - cos_y2_2;
  const double h = (cube(M)*y[1]*hair)/sqr(y1_2 + a_2*cos_y2_2);

  metric[0]  = -1 - h + (2*(1 + h)*M*y[1])/(y1_2 + a_2*cos_y2_2);
  metric[3]  = (-2*a*(1 + h)*M*y[1]*sin_y2_2)/(y1_2 + a_2*cos_y2_2);
  metric[5]  = ((1 + h)*(y1_2 + a_2*cos_y2_2))/(a_2 + y[1]*(-2*M + y[1]) + a_2*h*sin_y2_2);
  metric[10] = y1_2 + a_2*cos_y2_2;
  metric[12] = metric[3];
  metric[15] = sin_y2_2*(a_2 + y1_2 + (a_2*(y[1]*(2*(1 + h)*M + h*y[1]) + a_2*h*cos_y2_2)*sin_y2_2)/(y1_2 + a_2*cos_y2_2));
}


void KerrMetric::CalculateMetricElements3(double M, double a, double hair, const double *y, std::vector<double> &metric)
{
  const double a_2 = sqr(a);
  const double y1_2 = sqr(y[1]);
  const double cos_y2_2 = sqr(cos(y[2]));
  const double sin_y2_2 = 1 - cos_y2_2;
  const double h = (cube(M)*y[1]*hair)/sqr(y1_2 + a_2*cos_y2_2);

  metric[0]  = -1 - h + (2*(1 + h)*M*y[1])/(y1_2 + a_2*cos_y2_2);
  metric[3]  = (-2*a*(1 + h)*M*y[1]*sin_y2_2)/(y1_2 + a_2*cos_y2_2);
  metric[12] = metric[3];
  metric[15] = sin_y2_2*(a_2 + y1_2 + (a_2*(y[1]*(2*(1 + h)*M + h*y[1]) + a_2*h*cos_y2_2)*sin_y2_2)/(y1_2 + a_2*cos_y2_2));
}
