#ifndef XTRACK_KERR_H_INCLUDED
#define XTRACK_KERR_H_INCLUDED

#include <vector>

class KerrMetric {
public:
  static void Initialize(double hair);
  static void CalculateChristoffelSymbols(double M, double a, double hair, const double *y, std::vector<double> &symbols);
  static void CalculateChristoffelSymbolsNoHair(double M, double a, double, const double *y, std::vector<double> &symbols);
  static void CalculateMetricElements(double M, double a, double hair, const double *y, std::vector<double> &metric);
  static void CalculateMetricElements3(double M, double a, double hair, const double *y, std::vector<double> &metric);
};


#endif // XTRACK_KERR_H_INCLUDED
