#ifndef XTRACK_ALIEV_H_INCLUDED
#define XTRACK_ALIEV_H_INCLUDED

#include <vector>

class AlievMetric {
public:
  static void Initialize(double);
  static void CalculateChristoffelSymbols(double M, double a, double hair, const double *y, std::vector<double> &symbols);
  static void CalculateMetricElements(double M, double a, double hair, const double *y, std::vector<double> &metric);
  static void CalculateMetricElements3(double M, double a, double hair, const double *y, std::vector<double> &metric);
};


#endif // XTRACK_ALIEV_H_INCLUDED
