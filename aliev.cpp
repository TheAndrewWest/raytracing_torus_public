#include "aliev.h"
#include "metric.h"
#include "xTrackMath.h"

#include <cmath>
#include <iostream>


void AlievMetric::Initialize(double)
{
  ::CalculateMetricElements = &AlievMetric::CalculateMetricElements;
  ::CalculateMetricElements3 = &AlievMetric::CalculateMetricElements3;
  ::CalculateChristoffelSymbols = &AlievMetric::CalculateChristoffelSymbols;
}
  

void AlievMetric::CalculateChristoffelSymbols(double M, double a, double hair, const double *y, std::vector<double> &symbols)
{
  const double a_2 = a*a;
  const double a_3 = a_2*a;
  const double a_4 = a_3*a;
  const double a_6 = a_4*a_2;
  const double y1 = y[1];
  const double y1_2 = y1*y1;
  const double y1_3 = y1_2*y1;
  const double y1_4 = y1_3*y1;
  const double y1_5 = y1_4*y1;
  const double y1_6 = y1_5*y1;
  const double y2 = y[2];
  const double sin_y2 = sin(y2);
  const double sin_y2_2 = sin_y2*sin_y2;
  const double sin_y2_3 = sin_y2_2*sin_y2;
  const double cos_y2 = cos(y2);
  const double cos_y2_2 = cos_y2*cos_y2;
  const double cos_2y2 = cos_y2_2 - sin_y2_2;     // cos(2x) = cos^2(x) - sin^2(x)
  const double cos_4y2 = 2*cos_2y2*cos_2y2 - 1;   // cos(4x) = 2cos^2(2x) - 1
  const double cot_y2 = cos_y2/sin_y2;
  const double sin_2y2 = 2*sin_y2*cos_y2;         // sin(2x) = 2*sin(x)*cos(x)
  const double _y1_2_p_a_2cos_y2_2 = y1_2 + a_2*cos_y2_2;
  const double pow2_y1_2_p_a_2cos_y2_2 = sqr(_y1_2_p_a_2cos_y2_2);
  const double pow3_y1_2_p_a_2cos_y2_2 = pow2_y1_2_p_a_2cos_y2_2*_y1_2_p_a_2cos_y2_2;
  const double _2y1_2_p_a_2_p_a_2cos_2y2 = 2*y1_2 + a_2 + a_2*cos_2y2;
  const double pow2_2y1_2_p_a_2_p_a_2cos_2y2 = sqr(_2y1_2_p_a_2_p_a_2cos_2y2);
  
  symbols[1] = symbols[4] = (-2*(y1_2 + a_2)*(a_2*M + 2*y[1]*(hair - y[1]*M) + a_2*M*cos_2y2))/((a_2 + hair + y[1]*(y[1] - 2*M))*pow2_2y1_2_p_a_2_p_a_2cos_2y2);

  symbols[2] = (2*a_2*(hair - 2*y[1]*M)*sin_2y2)/pow2_2y1_2_p_a_2_p_a_2cos_2y2;

  symbols[7] = symbols[13] = (2*a*(a_4*M + 2*y1_3*(2*hair - 3*y[1]*M) + 3*y[1]*a_2*(hair - y[1]*M) + a_2*(a_2*M + y[1]*(hair - y[1]*M))*cos_2y2)*sin_y2_2)/((a_2 + hair + y[1]*(y[1] - 2*M))*pow2_2y1_2_p_a_2_p_a_2cos_2y2);

  symbols[8] = (2*a_2*(hair - 2*y[1]*M)*sin_2y2)/pow2_2y1_2_p_a_2_p_a_2cos_2y2;

  symbols[11] = symbols[14] = (-4*a_3*(hair - 2*y[1]*M)*cos_y2*sin_y2_3)/pow2_2y1_2_p_a_2_p_a_2cos_2y2;

  symbols[16] = -(((a_2 + hair + y[1]*(y[1] - 2*M))*(y[1]*(hair - y[1]*M) + a_2*M*cos_y2_2))/pow3_y1_2_p_a_2cos_y2_2);

  symbols[19] = symbols[28] = (a*(a_2 + hair + y[1]*(y[1] - 2*M))*(y[1]*(hair - y[1]*M) + a_2*M*cos_y2_2)*sin_y2_2)/pow3_y1_2_p_a_2cos_y2_2;

  symbols[21] = (-y[1] + M)/(y1_2 + a_2 + hair - 2*y[1]*M) + y[1]/(y1_2 + a_2*cos_y2_2);

  symbols[22] = -((a_2*cos_y2*sin_y2)/(y1_2 + a_2*cos_y2_2));

  symbols[25] = -((a_2*cos_y2*sin_y2)/(y1_2 + a_2*cos_y2_2));

  symbols[26] = -((y[1]*(a_2 + hair + y[1]*(y[1] - 2*M)))/(y1_2 + a_2*cos_y2_2));
	
  symbols[31] = -(((a_2 + hair + y[1]*(y[1] - 2*M))*sin_y2_2*(y[1]*pow2_y1_2_p_a_2cos_y2_2 + a_2*(y[1]*(hair - y[1]*M) + a_2*M*cos_y2_2)*sin_y2_2))/pow3_y1_2_p_a_2cos_y2_2);

  symbols[32] = (a_2*(hair - 2*y[1]*M)*cos_y2*sin_y2)/pow3_y1_2_p_a_2cos_y2_2;

  symbols[35] = symbols[44] = -((a*(y1_2 + a_2)*(hair - 2*y[1]*M)*cos_y2*sin_y2)/pow3_y1_2_p_a_2cos_y2_2);

  symbols[37] = (a_2*cos_y2*sin_y2)/((a_2 + hair + y[1]*(y[1] - 2*M))*(y1_2 + a_2*cos_y2_2));

  symbols[38] = symbols[41] = y[1]/(y1_2 + a_2*cos_y2_2);

  symbols[42] = -((a_2*cos_y2*sin_y2)/(y1_2 + a_2*cos_y2_2));

  symbols[47] = -(cos_y2*(8*y1_6 + 3*a_6 + 8*y1_2*a_2*(-hair + 2*y[1]*(y[1] + M)) + a_4*(-5*hair + y[1]*(11*y[1] + 10*M)) + a_2*(a_2 + hair + y[1]*(y[1] - 2*M))*(4*(2*y1_2 + a_2)*cos_2y2 + a_2*cos_4y2))*sin_y2)/(8.*pow3_y1_2_p_a_2cos_y2_2);

  symbols[49] = symbols[52] = (-4*a*(y[1]*(hair - y[1]*M) + a_2*M*cos_y2_2))/((a_2 + hair + y[1]*(y[1] - 2*M))*pow2_2y1_2_p_a_2_p_a_2cos_2y2);

  symbols[50] = (4*a*(hair - 2*y[1]*M)*cot_y2)/pow2_2y1_2_p_a_2_p_a_2cos_2y2;

  symbols[55] = symbols[61] = (4*y[1]*(y1_2 + a_2*cos_y2_2)*(hair + y[1]*(y[1] - 2*M) + a_2*cos_y2_2) + 4*a_2*(y[1]*(hair - y[1]*M) + a_2*M*cos_y2_2)*sin_y2_2)/((a_2 + hair + y[1]*(y[1] - 2*M))*pow2_2y1_2_p_a_2_p_a_2cos_2y2);

  symbols[56] = (4*a*(hair - 2*y[1]*M)*cot_y2)/pow2_2y1_2_p_a_2_p_a_2cos_2y2;

  symbols[59] = cot_y2 - (2*a_2*(hair - 2*y[1]*M)*sin_2y2)/pow2_2y1_2_p_a_2_p_a_2cos_2y2;

  symbols[62] = cot_y2 - (2*a_2*(hair - 2*y[1]*M)*sin_2y2)/pow2_2y1_2_p_a_2_p_a_2cos_2y2;
}


void AlievMetric::CalculateMetricElements(double M, double a, double hair, const double *y, std::vector<double> &metric)
{
  const double a_2 = sqr(a);
  const double y1_2 = sqr(y[1]);
  const double cos_y2_2 = sqr(cos(y[2]));
  const double sin_y2_2 = 1 - cos_y2_2;

  metric[0] = -1 - (hair - 2*y[1]*M)/(y1_2 + a_2*cos_y2_2);
  metric[3] = (a*(hair - 2*y[1]*M)*sin_y2_2)/(y1_2 + a_2*cos_y2_2);
  metric[5] = (y1_2 + a_2*cos_y2_2)/(y1_2 + a_2 + hair - 2*y[1]*M);
  metric[10] = y1_2 + a_2*cos_y2_2;
  metric[12] = (a*(hair - 2*y[1]*M)*sin_y2_2)/(y1_2 + a_2*cos_y2_2);
  metric[15] = sin_y2_2*(y1_2 + a_2 - (a_2*(hair - 2*y[1]*M)*sin_y2_2)/(y1_2 + a_2*cos_y2_2));
}


void AlievMetric::CalculateMetricElements3(double M, double a, double hair, const double *y, std::vector<double> &metric)
{
  const double a_2 = sqr(a);
  const double y1_2 = sqr(y[1]);
  const double cos_y2_2 = sqr(cos(y[2]));
  const double sin_y2_2 = 1 - cos_y2_2;

  metric[0] = -1 - (hair - 2*y[1]*M)/(y1_2 + a_2*cos_y2_2);
  metric[3] = (a*(hair - 2*y[1]*M)*sin_y2_2)/(y1_2 + a_2*cos_y2_2);
  metric[12] = (a*(hair - 2*y[1]*M)*sin_y2_2)/(y1_2 + a_2*cos_y2_2);
  metric[15] = sin_y2_2*(y1_2 + a_2 - (a_2*(hair - 2*y[1]*M)*sin_y2_2)/(y1_2 + a_2*cos_y2_2));
}
